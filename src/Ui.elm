module Ui exposing (externalLink, h1, head, nl, ul)

import Config
import Head
import Head.Seo as Seo
import Html exposing (..)
import Html.Attributes as HA
import Pages.Url
import Route


nl : Html msg
nl =
    br [] []


h1 : List (Attribute msg) -> List (Html msg) -> Html msg
h1 attributes children =
    Html.h1 attributes
        [ Route.link [] children Route.Index
        ]


externalLink : String -> String -> Html msg
externalLink url title =
    a
        [ HA.href url
        , HA.target "_blank"
        , HA.rel "noopener noreferrer nofollow"
        ]
        [ text title ]


ul : List (Attribute msg) -> List (Html msg) -> Html msg
ul attrs items =
    items
        |> List.map (\it -> li [] [ it ])
        |> Html.ul attrs


head :
    { title : String
    , description : String
    }
    -> List Head.Tag
head config =
    Seo.summary
        { canonicalUrlOverride = Nothing
        , siteName = Config.name
        , image =
            { url = Pages.Url.external "TODO"
            , alt = Config.name ++ " Logo"
            , dimensions = Nothing
            , mimeType = Nothing
            }
        , description = config.description
        , locale = Nothing
        , title = config.title
        }
        |> Seo.profile Config.profile
