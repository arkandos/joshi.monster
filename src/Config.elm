module Config exposing (..)

import BackendTask exposing (BackendTask)
import BackendTask.Env
import FatalError exposing (FatalError)


name =
    "joshi.monster"


canonicalUrl =
    "https://joshi.monster"


description =
    "joshi's webbed page"


profile =
    { firstName = "Joshua"
    , lastName = "Reusch"
    , username = Just "@jreusch@layer8.space"
    }

codeTheme =
    "nord"

fullName : String
fullName =
    profile.firstName ++ " " ++ profile.lastName


type alias EnvVars =
    { contentPath : String }

envVars : BackendTask FatalError EnvVars
envVars =
    BackendTask.map EnvVars
        (BackendTask.Env.expect "APP_CONTENT_DIR" |> BackendTask.allowFatal)
