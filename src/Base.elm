module Base exposing (..)

import BackendTask
import Effect
import FatalError
import Head
import PagesMsg
import RouteBuilder
import Shared
import UrlPath
import View


type alias BackendTask error value =
    BackendTask.BackendTask error value


type alias Effect msg =
    Effect.Effect msg


type alias FatalError =
    FatalError.FatalError


type alias Head =
    List Head.Tag


type alias PagesMsg msg =
    PagesMsg.PagesMsg msg


type alias UrlPath =
    UrlPath.UrlPath


type alias App data action routeParams =
    RouteBuilder.App data action routeParams


type alias StatelessRoute routeParams data action =
    RouteBuilder.StatelessRoute routeParams data action


type alias StatefulRoute routeParams data action model msg =
    RouteBuilder.StatefulRoute routeParams data action model msg


type alias View msg =
    View.View msg


type alias SharedModel =
    Shared.Model


noData : BackendTask error {}
noData =
    BackendTask.succeed {}


single =
    RouteBuilder.single


preRender =
    RouteBuilder.preRender


buildNoState =
    RouteBuilder.buildNoState


buildWithLocalState =
    RouteBuilder.buildWithLocalState


type alias StaticPage data =
    StatelessRoute {} data {}


staticPage :
    { head : data -> Head
    , data : BackendTask FatalError data
    , view : data -> View Never
    }
    -> StaticPage data
staticPage config =
    RouteBuilder.single
        { head = \app -> config.head app.data
        , data = config.data
        }
        |> RouteBuilder.buildNoState
            { view = \app _ -> View.map never (config.view app.data)
            }


staticPages :
    { head : routeParams -> data -> Head
    , pages : BackendTask FatalError (List routeParams)
    , data : routeParams -> BackendTask FatalError data
    , view : routeParams -> data -> View Never
    }
    -> StatelessRoute routeParams data {}
staticPages config =
    RouteBuilder.preRender
        { head = \app -> config.head app.routeParams app.data
        , pages = config.pages
        , data = config.data
        }
        |> RouteBuilder.buildNoState
            { view = \app _ -> View.map never (config.view app.routeParams app.data)
            }


type alias DynamicPage data model msg =
    StatefulRoute {} data {} model msg


dynamicPage :
    { head : data -> Head
    , data : BackendTask FatalError data
    , init : data -> ( model, Effect msg )
    , update : data -> msg -> model -> ( model, Effect msg )
    , subscriptions : model -> Sub msg
    , view : data -> model -> View msg
    }
    -> DynamicPage data model msg
dynamicPage config =
    RouteBuilder.single
        { head = \app -> config.head app.data
        , data = config.data
        }
        |> RouteBuilder.buildWithLocalState
            { init = \app _ -> config.init app.data
            , update = \app _ msg model -> config.update app.data msg model
            , subscriptions = \_ _ _ model -> config.subscriptions model
            , view = \app _ model -> View.map PagesMsg.fromMsg (config.view app.data model)
            }


dynamicPages :
    { head : routeParams -> data -> Head
    , pages : BackendTask FatalError (List routeParams)
    , data : routeParams -> BackendTask FatalError data
    , init : routeParams -> data -> ( model, Effect msg )
    , update : routeParams -> data -> msg -> model -> ( model, Effect msg )
    , subscriptions : routeParams -> model -> Sub msg
    , view : routeParams -> data -> model -> View msg
    }
    -> StatefulRoute routeParams data {} model msg
dynamicPages config =
    RouteBuilder.preRender
        { head = \app -> config.head app.routeParams app.data
        , pages = config.pages
        , data = config.data
        }
        |> RouteBuilder.buildWithLocalState
            { init = \app _ -> config.init app.routeParams app.data
            , update = \app _ msg model -> config.update app.routeParams app.data msg model
            , subscriptions = \routeParams _ _ model -> config.subscriptions routeParams model
            , view = \app _ model -> View.map PagesMsg.fromMsg (config.view app.routeParams app.data model)
            }


noEffect : Effect msg
noEffect =
    Effect.none


fromCmd : Cmd msg -> Effect msg
fromCmd =
    Effect.fromCmd
