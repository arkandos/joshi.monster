module Rss exposing (generate, Item, Enclosure)

{-| Build a feed following the RSS 2.0 format <https://validator.w3.org/feed/docs/rss2.html>.
<http://www.rssboard.org/rss-specification>

@docs generate, Item, DateOrTime, Enclosure

-}

import Date exposing (Date)
import Dict
import Time
import Xml
import Xml.Encode exposing (..)


{-| Data representing an RSS feed item.

contentEncoded - Use this to
[add HTML content](https://developer.mozilla.org/en-US/docs/Archive/RSS/Article/Why_RSS_Content_Module_is_Popular_-_Including_HTML_Contents)
in a `<content:encoded>` tag in the RSS feed. Some feed readers
will use this field if present to render HTML. Note that Elm doesn't
provide a way to turn `Html.Html` values into a `String`.

You can use [`zwilias/elm-html-string`](https://package.elm-lang.org/packages/zwilias/elm-html-string/latest/) to
render HTML using a drop-in replacement API and then turn that into a String.

Here's an example that shows how to [render to an HTML String
using `dillonkearns/elm-markdown`](https://github.com/dillonkearns/elm-markdown/blob/2650722990d61c8948d7998168d3bceb0ee6f298/spec-tests/OutputMarkdownHtml.elm).

<https://demo.ghost.io/author/lewis/rss/>

Encoding

enclosure - link to an attached file

<https://www.rssboard.org/rss-enclosures-use-case>

-}
type alias Item =
    { title : String
    , description : String
    , url : String
    , categories : List String
    , author : String
    , pubDate : Date
    , content : Maybe String
    , contentEncoded : Maybe String
    , enclosure :
        Maybe
            { url : String
            , mimeType : String
            , bytes : Maybe Int
            }

    {-
       TODO consider adding these
          - lat optional number The latitude coordinate of the item.
          - long optional number The longitude coordinate of the item.
          - custom_elements optional array Put additional elements in the item (node-xml syntax)
    -}
    }


{-| Represents a linked file.

<https://validator.w3.org/feed/docs/rss2.html#ltenclosuregtSubelementOfLtitemgt>

-}
type alias Enclosure =
    { url : String
    , mimeType : String
    , bytes : Maybe Int
    }


{-| Generate an RSS feed from feed metadata and a list of `Rss.Item`s.
-}
generate :
    { title : String
    , description : String
    , url : String
    , lastBuildTime : Time.Posix
    , generator : Maybe String
    , items : List Item
    , siteUrl : String
    }
    -> String
generate feed =
    object
        [ ( "rss"
          , Dict.fromList
                [ ( "xmlns:dc", string "http://purl.org/dc/elements/1.1/" )
                , ( "xmlns:content", string "http://purl.org/rss/1.0/modules/content/" )
                , ( "xmlns:atom", string "http://www.w3.org/2005/Atom" )
                , ( "version", string "2.0" )
                ]
          , object
                [ ( "channel"
                  , Dict.empty
                  , [ [ keyValue "title" feed.title
                      , keyValue "description" feed.description
                      , keyValue "link" feed.url

                      --<atom:link href="http://dallas.example.com/rss.xml" rel="self" type="application/rss+xml" />
                      , keyValue "lastBuildDate" <| formatDateTime feed.lastBuildTime
                      ]
                    , [ feed.generator |> Maybe.map (keyValue "generator") ] |> List.filterMap identity
                    , List.map (itemXml feed.siteUrl) feed.items
                    ]
                        |> List.concat
                        |> list
                  )
                ]
          )
        ]
        |> Xml.Encode.encode 0


itemXml : String -> Item -> Xml.Value
itemXml siteUrl item =
    object
        [ ( "item"
          , Dict.empty
          , list
                ([ keyValue "title" item.title
                 , keyValue "description" item.description
                 , keyValue "link" (joinUrl [ siteUrl, item.url ])
                 , keyValue "guid" (joinUrl [ siteUrl, item.url ])
                 , keyValue "pubDate" (formatDate item.pubDate)
                 ]
                    ++ List.map encodeCategory item.categories
                    ++ ([ item.content |> Maybe.map (\content -> keyValue "content" content)
                        , item.contentEncoded |> Maybe.map (\content -> keyValue "content:encoded" (wrapInCdata content))
                        , item.enclosure |> Maybe.map encodeEnclosure

                        --<enclosure url="https://example.com/image.jpg" length="0" type="image/jpeg"/>
                        ]
                            |> List.filterMap identity
                       )
                )
          )
        ]


encodeCategory : String -> Xml.Value
encodeCategory category =
    Xml.Encode.object
        [ ( "category", Dict.empty, Xml.Encode.string category )
        ]


encodeEnclosure : Enclosure -> Xml.Value
encodeEnclosure enclosure =
    Xml.Encode.object
        [ ( "enclosure"
          , Dict.fromList
                [ ( "url", string enclosure.url )
                , ( "length", string "0" )
                , ( "type", string enclosure.mimeType )
                ]
          , Xml.Encode.null
          )
        ]


wrapInCdata content =
    "<![CDATA[" ++ content ++ "]]>"


formatDate : Date -> String
formatDate date =
    Date.format "EEE, dd MMM yyyy" date
        ++ " 00:00:00 GMT"


formatDateTime : Time.Posix -> String
formatDateTime posix =
    let
        date =
            Date.fromPosix Time.utc posix

        hours =
            Time.toHour Time.utc posix

        minutes =
            Time.toMinute Time.utc posix

        seconds =
            Time.toSecond Time.utc posix

        fmt : Int -> String
        fmt part =
            if part < 10 then
                "0" ++ String.fromInt part

            else
                String.fromInt part
    in
    Date.format "EEE, dd MMM yyyy" date
        ++ " "
        ++ fmt hours
        ++ ":"
        ++ fmt minutes
        ++ ":"
        ++ fmt seconds
        ++ " GMT"


keyValue : String -> String -> Xml.Value
keyValue key value =
    object [ ( key, Dict.empty, string value ) ]


joinUrl : List String -> String
joinUrl parts =
    let
        trimStart str =
            if String.startsWith "/" str then
                String.dropLeft 1 str

            else
                str

        trimEnd str =
            if String.endsWith "/" str then
                String.dropRight 1 str

            else
                str

        trimBoth str =
            str |> trimStart |> trimEnd
    in
    parts
        |> List.map trimBoth
        |> String.join "/"
