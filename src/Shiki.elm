module Shiki exposing (Highlighted, ShikiToken, highlight, toString, view)

import BackendTask exposing (BackendTask)
import BackendTask.Custom
import Config
import FatalError exposing (FatalError)
import Html exposing (Html)
import Html.Attributes as Attr exposing (class)
import Html.Events
import Icons
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Extra as Decode
import Json.Encode as Encode
import Json.Encode.Extra as Encode


type alias ShikiToken =
    { content : String
    , color : Maybe String
    , fontStyle : Maybe ( String, String )
    }


type alias Highlighted =
    { lines : List (List ShikiToken)
    , fg : String
    , bg : String
    }


highlight : { code : String, lang : Maybe String } -> BackendTask FatalError Highlighted
highlight { code, lang } =
    let
        jsonArg =
            Encode.object
                [ ( "code", Encode.string code )
                , ( "lang", Encode.maybe Encode.string lang )
                , ( "theme", Encode.string Config.codeTheme )
                ]
    in
    BackendTask.Custom.run "highlightCode" jsonArg decoder
        |> BackendTask.map (\highlighted -> { highlighted | lines = trimLines highlighted.lines 0 [] })
        |> BackendTask.quiet
        |> BackendTask.allowFatal


decoder : Decoder Highlighted
decoder =
    Decode.map3 Highlighted
        (Decode.field "tokens" (Decode.list (Decode.list shikiTokenDecoder)))
        (Decode.field "fg" Decode.string)
        (Decode.field "bg" Decode.string)


shikiTokenDecoder : Decode.Decoder ShikiToken
shikiTokenDecoder =
    Decode.map3 ShikiToken
        (Decode.field "content" Decode.string)
        (Decode.optionalField "color" Decode.string)
        (Decode.optionalField "fontStyle" fontStyleDecoder |> Decode.map (Maybe.andThen identity))


fontStyleDecoder : Decoder (Maybe ( String, String ))
fontStyleDecoder =
    Decode.int
        |> Decode.map
            (\styleNumber ->
                case styleNumber of
                    1 ->
                        Just ( "font-style", "italic" )

                    2 ->
                        Just ( "font-style", "bold" )

                    4 ->
                        Just ( "font-style", "underline" )

                    _ ->
                        Nothing
            )


{-| <https://github.com/shikijs/shiki/blob/2a31dc50f4fbdb9a63990ccd15e08cccc9c1566a/packages/shiki/src/renderer.ts#L16>
-}
view : Maybe (String -> msg) -> Highlighted -> Html msg
view maybeCopyToClipboard highlighted =
    Html.pre
        [ Attr.style "background-color" highlighted.bg
        , Attr.style "white-space" "pre-wrap"
        , Attr.style "overflow-wrap" "break-word"
        ]
        [ case maybeCopyToClipboard of
            Just copyToClipboard ->
                case highlighted.lines of
                    -- at least 2 lines
                    _ :: _ :: _ ->
                        Html.button
                            [ Html.Events.onClick (copyToClipboard <| toString highlighted)
                            , Attr.class "copy-to-clipboard"
                            ]
                            [ Icons.clipboard ]

                    _ ->
                        Html.text ""

            Nothing ->
                Html.text ""
        , Html.code [] (viewLines highlighted)
        ]


viewLines : Highlighted -> List (Html msg)
viewLines highlighted =
    highlighted.lines
        |> List.concatMap
            (\line ->
                [ Html.span [ class "line" ]
                    (line
                        |> List.map
                            (\token ->
                                Html.span
                                    [ Attr.style "color" (token.color |> Maybe.withDefault highlighted.fg)
                                    , token.fontStyle
                                        |> Maybe.map
                                            (\( key, value ) ->
                                                Attr.style key value
                                            )
                                        |> Maybe.withDefault (Attr.title "")
                                    ]
                                    [ Html.text token.content ]
                            )
                    )
                , Html.text "\n"
                ]
            )


toString : Highlighted -> String
toString { lines } =
    lines
        |> List.map
            (\line ->
                line
                    |> List.map .content
                    |> String.join ""
            )
        |> String.join "\n"


trimLines : List (List ShikiToken) -> Int -> List (List ShikiToken) -> List (List ShikiToken)
trimLines lines emptyLines acc =
    case lines of
        [] ->
            -- implicitely drop empty lines at the end
            List.reverse acc

        line :: rest ->
            if List.isEmpty line then
                if List.isEmpty acc then
                    -- drop empty lines at the start
                    trimLines rest emptyLines acc

                else
                    trimLines rest (emptyLines + 1) acc

            else
                trimLines rest 0 (line :: prependEmptyLines emptyLines acc)


prependEmptyLines : Int -> List (List ShikiToken) -> List (List ShikiToken)
prependEmptyLines n acc =
    if n > 0 then
        prependEmptyLines (n - 1) ([] :: acc)

    else
        acc
