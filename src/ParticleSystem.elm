module ParticleSystem exposing (Msg, Particle, ParticleSystem, constant, init, random, spawnNewParticles, subscriptions, update)

import Browser.Events exposing (Visibility(..))
import Random exposing (Generator)


type Msg
    = GotSeed Random.Seed
    | VisibilityChanged Visibility
    | Tick Float


type alias Particle =
    { x : Float
    , y : Float
    , a : Float
    , dx : Float
    , dy : Float
    , da : Float
    , ddx : Float
    , ddy : Float
    , dda : Float
    , lifespan : Float
    , age : Float
    }


updateParticle : Float -> Particle -> Maybe Particle
updateParticle ms p =
    let
        t =
            ms / 1000

        dx =
            p.dx + p.ddx * t

        dy =
            p.dy + p.ddy * t

        da =
            p.da + p.dda * t
    in
    if p.lifespan > t then
        Just
            { x = p.x + dx * t
            , y = p.y + dy * t
            , a = p.a + da * t
            , dx = dx
            , dy = dy
            , da = da
            , ddx = p.ddx
            , ddy = p.ddy
            , dda = p.dda
            , lifespan = p.lifespan - t
            , age = p.age + t
            }

    else
        Nothing


type Parameter
    = Static Float
    | Uniform Float Float


constant : Float -> Parameter
constant =
    Static


random : Float -> Float -> Parameter
random =
    Uniform


type alias ParticleSystem =
    { particles : List Particle

    -- internal
    , seed : Random.Seed
    , ts : Float
    , nextSpawn : Float
    , visibility : Visibility

    -- spawner params
    , spawnFrequency : Parameter

    -- spawn particle params
    , x : Parameter
    , y : Parameter
    , a : Parameter
    , dx : Parameter
    , dy : Parameter
    , da : Parameter
    , ddx : Parameter
    , ddy : Parameter
    , dda : Parameter
    , lifespan : Parameter
    }


init :
    { toMsg : Msg -> msg
    , spawnFrequency : Parameter

    -- spawn particle params
    , x : Parameter
    , y : Parameter
    , a : Parameter
    , dx : Parameter
    , dy : Parameter
    , da : Parameter
    , ddx : Parameter
    , ddy : Parameter
    , dda : Parameter
    , lifespan : Parameter
    }
    -> ( ParticleSystem, Cmd msg )
init config =
    ( { particles = []
      , seed = Random.initialSeed 0
      , ts = 0
      , nextSpawn = 0
      , visibility = Visible
      , spawnFrequency = config.spawnFrequency
      , x = config.x
      , y = config.y
      , a = config.a
      , dx = config.dx
      , dy = config.dy
      , da = config.da
      , ddx = config.ddx
      , ddy = config.ddy
      , dda = config.dda
      , lifespan = config.lifespan
      }
    , Random.generate (config.toMsg << GotSeed) Random.independentSeed
    )


spawnNewParticles : Int -> ParticleSystem -> ParticleSystem
spawnNewParticles count ps =
    if count == 0 then
        ps

    else
        spawnNewParticles (count - 1) (spawnNewParticle ps)


update : Msg -> ParticleSystem -> ParticleSystem
update msg ps =
    case msg of
        GotSeed seed ->
            { ps | seed = seed }

        VisibilityChanged visibility ->
            { ps | visibility = visibility }

        Tick ms ->
            let
                ts =
                    ps.ts + ms / 1000
            in
            { ps | ts = ts }
                |> trySpawnParticle
                |> updateParticles ms


subscriptions : (Msg -> msg) -> ParticleSystem -> Sub msg
subscriptions toMsg ps =
    Sub.batch
        [ Browser.Events.onVisibilityChange (toMsg << VisibilityChanged)
        , case ps.visibility of
            Visible ->
                Browser.Events.onAnimationFrameDelta (toMsg << Tick)

            Hidden ->
                Sub.none
        ]


trySpawnParticle : ParticleSystem -> ParticleSystem
trySpawnParticle ps =
    if ps.ts > ps.nextSpawn then
        trySpawnParticle (spawnNewParticle ps)

    else
        ps


spawnNewParticle : ParticleSystem -> ParticleSystem
spawnNewParticle ps =
    let
        ( newParticle, seed1 ) =
            Random.step (particleGenerator ps) ps.seed

        ( nextSpawnDelta, seed2 ) =
            Random.step (paramGenerator ps.spawnFrequency) seed1
    in
    { ps
        | particles = newParticle :: ps.particles
        , seed = seed2
        , nextSpawn = ps.ts + nextSpawnDelta
    }


updateParticles : Float -> ParticleSystem -> ParticleSystem
updateParticles ms system =
    { system | particles = List.filterMap (updateParticle ms) system.particles }


particleGenerator : ParticleSystem -> Generator Particle
particleGenerator system =
    Random.constant Particle
        |> withParam system.x
        |> withParam system.y
        |> withParam system.a
        |> withParam system.dx
        |> withParam system.dy
        |> withParam system.da
        |> withParam system.ddx
        |> withParam system.ddy
        |> withParam system.dda
        |> withParam system.lifespan
        |> andMap (Random.constant 0)


withParam : Parameter -> Generator (Float -> a) -> Generator a
withParam param =
    andMap (paramGenerator param)


paramGenerator : Parameter -> Generator Float
paramGenerator param =
    case param of
        Static value ->
            Random.constant value

        Uniform min max ->
            Random.float min max


andMap : Generator a -> Generator (a -> b) -> Generator b
andMap =
    Random.map2 (|>)
