module Content exposing
    ( Content(..)
    , Link
    , Metadata
    , Post
    , Text
    , link
    , listAll
    , listPostMetadata
    , listSlugs
    , loadLinks
    , loadMarkdownText
    , loadPost
    , publishedAt
    , url
    , view
    , viewContentLink
    )

import BackendTask exposing (BackendTask)
import BackendTask.File
import BackendTask.Glob as Glob
import Config
import Date exposing (Date)
import FatalError exposing (FatalError)
import Form.Field exposing (date)
import Html exposing (Attribute, Html, li, p, text)
import Html.Attributes exposing (class)
import Json.Decode as Json exposing (Decoder)
import Json.Decode.Extra as Json
import Markdown.Block
import Markdown.Html
import Markdown.Parser
import Markdown.Renderer
import Parser exposing ((|.), (|=), Parser)
import Regex exposing (Regex)
import Route exposing (Route)
import Shiki
import Time exposing (Month(..))
import Ui
import UrlPath



-- NOTE: the slash at the end is important for glob matching!
--
-- LOADING CONTENT
--


linksFile : String
linksFile =
    "content/links.md"


contentRoute : { slug : String } -> Route.Route
contentRoute =
    Route.Posts__Slug_


type alias Post =
    { metadata : Metadata
    , text : Text
    }


type alias Metadata =
    { title : String
    , published : Date
    , summary : String
    , tags : List String
    , slug : String
    }


type alias Link =
    { published : Date
    , title : String
    , url : String
    , highlight : Bool
    }


type Content
    = PostContent Metadata
    | LinkContent Link


type alias Text =
    List Block


type Block
    = Markdown Markdown.Block.Block
    | Code Shiki.Highlighted


listAll : BackendTask FatalError (List Content)
listAll =
    BackendTask.map2 List.append
        (loadLinks |> BackendTask.map (List.map LinkContent))
        (listPostMetadata |> BackendTask.map (List.map PostContent))


listSlugs : BackendTask FatalError (List { filePath : String, slug : String })
listSlugs =
    let
        listDir contentPath =
            case String.split ":" contentPath of
                mainContentDir :: additionalContentDirs ->
                    Glob.succeed (\slug filePath -> { slug = slug, filePath = filePath })
                        |> Glob.match (oneOfOrLiteral mainContentDir additionalContentDirs)
                        |> Glob.capture Glob.wildcard
                        |> Glob.match
                            (Glob.oneOf
                                ( ( "", () )
                                , [ ( "/post", () ) ]
                                )
                            )
                        |> Glob.match (Glob.literal ".md")
                        |> Glob.captureFilePath
                        |> Glob.toBackendTask

                [] ->
                    FatalError.fromString "No content path set"
                        |> BackendTask.fail
    in
    Config.envVars
        |> BackendTask.andThen (\{ contentPath } -> listDir contentPath)


listPostMetadata : BackendTask FatalError (List Metadata)
listPostMetadata =
    listSlugs
        |> BackendTask.andThen (List.map loadPostMetadata >> BackendTask.combine)


loadPostMetadata : { slug : String, filePath : String } -> BackendTask FatalError Metadata
loadPostMetadata { slug, filePath } =
    BackendTask.File.onlyFrontmatter decoder filePath
        |> BackendTask.map ((|>) slug)
        |> BackendTask.allowFatal


loadPost : String -> BackendTask FatalError Post
loadPost slug =
    getFilePathFromSlug slug
        |> BackendTask.andThen
            (\filePath ->
                BackendTask.map2 Post
                    (loadPostMetadata { slug = slug, filePath = filePath })
                    (loadMarkdownText filePath)
            )


loadLinks : BackendTask FatalError (List Link)
loadLinks =
    BackendTask.File.bodyWithoutFrontmatter linksFile
        |> BackendTask.allowFatal
        |> BackendTask.andThen parseMarkdown
        |> BackendTask.andThen
            (\blocks ->
                case parseLinkBlocks blocks of
                    Ok ( _, lists ) ->
                        BackendTask.succeed lists

                    Err err ->
                        FatalError.build { title = "Error parsing links", body = err }
                            |> BackendTask.fail
            )



--
-- PARSING
--


decoder : Decoder (String -> Metadata)
decoder =
    Json.map4 Metadata
        (Json.field "title" Json.string)
        (Json.field "published" dateDecoder)
        (Json.optionalField "summary" Json.string |> Json.map (Maybe.withDefault ""))
        (Json.optionalField "tags" Json.string
            |> Json.map
                (\tagsStr ->
                    tagsStr
                        |> Maybe.withDefault ""
                        |> String.toLower
                        |> String.split ","
                        |> List.map String.trim
                        |> List.filter ((/=) "")
                )
        )


oneOfOrLiteral : String -> List String -> Glob.Glob ()
oneOfOrLiteral mainPath additionalPaths =
    -- Glob.oneOf breaks if called with empty additionalPaths
    if List.isEmpty additionalPaths then
        Glob.literal mainPath
            |> Glob.map (always ())

    else
        Glob.oneOf
            ( ( mainPath, () )
            , additionalPaths |> List.map (\dir -> ( dir, () ))
            )


getFilePathFromSlug : String -> BackendTask FatalError String
getFilePathFromSlug slug =
    let
        getFilePath contentPath =
            case String.split ":" contentPath of
                mainContentDir :: additionalContentDirs ->
                    Glob.succeed identity
                        |> Glob.captureFilePath
                        |> Glob.match (oneOfOrLiteral mainContentDir additionalContentDirs)
                        |> Glob.match (Glob.literal slug)
                        |> Glob.match
                            (Glob.oneOf
                                ( ( "", () )
                                , [ ( "/post", () ) ]
                                )
                            )
                        |> Glob.match (Glob.literal ".md")
                        |> Glob.expectUniqueMatch
                        |> BackendTask.allowFatal

                [] ->
                    FatalError.fromString "No content path set"
                        |> BackendTask.fail
    in
    Config.envVars
        |> BackendTask.andThen (\{ contentPath } -> getFilePath contentPath)


loadMarkdownText : String -> BackendTask FatalError Text
loadMarkdownText path =
    let
        parseBlock block =
            case block of
                Markdown.Block.CodeBlock { body, language } ->
                    Shiki.highlight { code = body, lang = language }
                        |> BackendTask.map Code

                _ ->
                    BackendTask.succeed (Markdown block)
    in
    BackendTask.File.bodyWithoutFrontmatter path
        |> BackendTask.allowFatal
        |> BackendTask.andThen parseMarkdown
        |> BackendTask.andThen (List.map parseBlock >> BackendTask.combine)


parseMarkdown : String -> BackendTask FatalError (List Markdown.Block.Block)
parseMarkdown rawMarkdown =
    Markdown.Parser.parse rawMarkdown
        |> Result.mapError
            (\errors ->
                let
                    body =
                        errors
                            |> List.map Markdown.Parser.deadEndToString
                            |> String.join "\n"
                in
                FatalError.build { title = "Markdown parsing error!", body = body }
            )
        |> BackendTask.fromResult


parseLinkBlocks : List Markdown.Block.Block -> Result String ( Maybe Date, List Link )
parseLinkBlocks =
    List.foldl (\block -> Result.andThen (\state -> parseLinkBlock block state)) (Ok ( Nothing, [] ))


parseLinkBlock : Markdown.Block.Block -> ( Maybe Date, List Link ) -> Result String ( Maybe Date, List Link )
parseLinkBlock block ( maybePublished, acc ) =
    case block of
        Markdown.Block.Heading _ inlines ->
            let
                str =
                    Markdown.Block.extractInlineText inlines
            in
            case Parser.run dateParser str of
                Ok date ->
                    Ok ( Just date, acc )

                Err _ ->
                    Err ("Invalid date: " ++ str)

        Markdown.Block.UnorderedList _ listItems ->
            case maybePublished of
                Just published ->
                    Ok ( maybePublished, parseLinkListItems published listItems acc )

                Nothing ->
                    Ok ( maybePublished, acc )

        _ ->
            Ok ( maybePublished, acc )


parseLinkListItems : Date -> List (Markdown.Block.ListItem Markdown.Block.Block) -> List Link -> List Link
parseLinkListItems published listItems acc =
    case listItems of
        (Markdown.Block.ListItem task itemBlocks) :: remaining ->
            case itemBlocks of
                [ Markdown.Block.Paragraph [ Markdown.Block.Link linkUrl _ inlines ] ] ->
                    let
                        newLink =
                            { published = published
                            , url = linkUrl
                            , title = Markdown.Block.extractInlineText inlines
                            , highlight = task == Markdown.Block.CompletedTask
                            }
                    in
                    parseLinkListItems published remaining (newLink :: acc)

                _ ->
                    parseLinkListItems published remaining acc

        [] ->
            acc


dateDecoder : Decoder Date
dateDecoder =
    let
        runDateParser str =
            case Parser.run dateParser str of
                Ok date ->
                    Json.succeed date

                Err _ ->
                    Json.fail <| "Invalid date: " ++ str
    in
    Json.andThen runDateParser Json.string


dateParser : Parser Date
dateParser =
    let
        make month str =
            Parser.map (always month) (Parser.keyword str)

        monthParser =
            Parser.oneOf
                [ make Jan "Jan"
                , make Feb "Feb"
                , make Mar "Mar"
                , make Apr "Apr"
                , make May "May"
                , make Jun "Jun"
                , make Jul "Jul"
                , make Aug "Aug"
                , make Sep "Sep"
                , make Oct "Oct"
                , make Nov "Nov"
                , make Dec "Dec"
                ]

        dayParser =
            Parser.succeed identity
                |. Parser.chompWhile (\c -> c == '0')
                |= Parser.int
    in
    Parser.succeed (\month day year -> Date.fromCalendarDate year month day)
        |. Parser.spaces
        |= monthParser
        |. Parser.spaces
        |= Parser.oneOf
            [ Parser.backtrackable dayParser
                |. Parser.symbol ","
                |. Parser.spaces
            , Parser.succeed 1
            ]
        |= Parser.int
        |. Parser.spaces
        |. Parser.end



--
-- CONTENT VIEW
--


view :
    { page : Route
    , content : List Block
    , attributes : List (Attribute msg)
    , copyToClipboard : Maybe (String -> msg)
    }
    -> Html msg
view config =
    let
        renderer =
            getRenderer config.page
    in
    config.content
        |> List.concatMap (viewBlock renderer config.copyToClipboard)
        |> Html.article config.attributes


type alias Renderer =
    Markdown.Renderer.Renderer (Html Never)


getRenderer : Route -> Renderer
getRenderer route =
    let
        resolveUrl theUrl =
            if
                String.startsWith "http://" theUrl
                    || String.startsWith "https://" theUrl
                    || String.startsWith "/" theUrl
            then
                theUrl

            else
                UrlPath.toAbsolute [ Route.toString route, theUrl ]

        defaultRenderer =
            Markdown.Renderer.defaultHtmlRenderer
    in
    { defaultRenderer
        | heading =
            \{ level, rawText, children } ->
                let
                    node =
                        case level of
                            Markdown.Block.H1 ->
                                Html.h1

                            Markdown.Block.H2 ->
                                Html.h2

                            Markdown.Block.H3 ->
                                Html.h3

                            Markdown.Block.H4 ->
                                Html.h4

                            Markdown.Block.H5 ->
                                Html.h5

                            Markdown.Block.H6 ->
                                Html.h6
                in
                node [ Html.Attributes.id (slugify rawText) ] children
        , image =
            \{ alt, src, title } ->
                Html.img
                    [ Html.Attributes.alt alt
                    , Html.Attributes.src (resolveUrl src)
                    , Html.Attributes.title (Maybe.withDefault alt title)
                    ]
                    []
        , link =
            \{ title, destination } content ->
                Html.a
                    [ Html.Attributes.href (resolveUrl destination)
                    , case title of
                        Just theTitle ->
                            Html.Attributes.title theTitle

                        Nothing ->
                            Html.Attributes.class ""
                    ]
                    content
        , html =
            Markdown.Html.oneOf
                [ Markdown.Html.tag "small"
                    (\text _ ->
                        Html.small [] [ Html.text text ]
                    )
                    |> Markdown.Html.withAttribute "text"
                , Markdown.Html.tag "badge"
                    (\text _ ->
                        Html.span [ class "badge" ] [ Html.text text ]
                    )
                    |> Markdown.Html.withAttribute "text"
                ]
    }


viewBlock : Renderer -> Maybe (String -> msg) -> Block -> List (Html msg)
viewBlock renderer copyToClipboard block =
    case block of
        Markdown markdown ->
            markdown
                |> List.singleton
                |> Markdown.Renderer.render renderer
                |> Result.withDefault []
                |> List.map (Html.map never)

        Code highlighted ->
            [ Shiki.view copyToClipboard highlighted ]


viewContentLink : { withSummary : Bool } -> Content -> Html msg
viewContentLink { withSummary } content =
    case content of
        PostContent metadata ->
            if withSummary && metadata.summary /= "" then
                li []
                    [ p [] [ link metadata.slug metadata.title ]
                    , p [] [ text metadata.summary ]
                    ]

            else
                li [] [ link metadata.slug metadata.title ]

        LinkContent theLink ->
            li [] [ Ui.externalLink theLink.url theLink.title ]


url : String -> String
url slug =
    contentRoute { slug = slug } |> Route.toString


link : String -> String -> Html msg
link slug title =
    contentRoute { slug = slug } |> Route.link [] [ Html.text title ]


publishedAt : Content -> Date
publishedAt content =
    case content of
        LinkContent { published } ->
            published

        PostContent { published } ->
            published


slugRegex : Regex
slugRegex =
    Regex.fromString "[^a-zA-Z0-9 ]+"
        |> Maybe.withDefault Regex.never


slugify : String -> String
slugify text =
    text
        |> String.toLower
        |> String.replace "'" ""
        |> Regex.replace slugRegex (always " ")
        |> String.split " "
        |> List.filter (String.isEmpty >> not)
        |> String.join "-"
