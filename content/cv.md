Captain of the cloud, master of the Linux console, debugging wizard, conqueror of every souls-like game. I will support your team fight the big bosses and overcome every challenge.

I'm especially skilled in learning and understanding new systems, technologies and domains quickly and deeply. My preferred role is sharing this knowledge with others, collaboratively solving complex problems and building tools and processes that enable everyone to reach their true potential. I strive to get everyone on board to share this same vision, and build genuinely nice and delightful experiences together, that also fullfill that same promise on a technical level.

While I am currently a student again and not available for full-time work, I am generally prioritizing remote positions at companies that share my values of kindness, transparency, honesty, and a culture that strives to learn something new every day.

 &mdash; Joshua Reusch

---

## creativITy GmbH <small text="2024-ongoing" />

 <badge text="C#" />
 <badge text="VB.NET" />
 <badge text="TS" />
 <badge text="PHP" />
 <badge text="Bash" />
 <badge text="Powershell" />
 <badge text="Node.JS" />
 <badge text="Docker" />
 <badge text="MS-SQL" />
 <badge text="MySQL" />
 <badge text="REST" />
 <badge text="OpenAPI" />
 <badge text="CI/CD" />
 <badge text="Jira" />
 <badge text="SBS" />

I have just started there, and will report back soon (tm)!

---

## audaris GmbH <small text="2018-2024" />


After my efforts to finish my bachelors degree, I joined audaris as an indivual contributor. In this role, I was responsible for maintaining and extending existing projects, including our app, the vehicle showroom, and the import/export system. All of these components required major updates to align with our new Dealer Management System (DMS), which was also being fully rewritten at the same time. During this period, I had the opportunity to experiment with various technologies, some of which worked out well, while others didn't. Collaborating closely with the DMS team, we decided to introduce Vue and Node.JS into our codebase.

After the initial release of the new systems, it became clear that we needed to establish better processes for releases, development, and testing. We also aimed to address potential reliability and scalability issues as our customers transitioned from the old to the new system. Following an internal restructuring of our teams, I took on the challenge of containerizing all of our services, setting up a Kubernetes cluster, implementing CI/CD, and defining a workflow based on Gitflow that suited our needs. Additionally, I introduced new team members to our codebase. This transitioned mostly smoothly into production after some initial hiccups, however, we encountered persistent challenges in finding a development setup that worked for all team members.

The inherent complexity of Kubernetes also proved to be difficult to communicate. While the cluster operated smoothly most of the time, I took the initiative to provide my team members with fundamental knowledge of Kubernetes Kubernetes, empowering them to deploy updates, previews, and new services using our CI/CD server and existing templates.

Nethertheless, ensuring that everyone knew how to respond to individual services or nodes going down proved to be a more significant challenge. While most team members understood the core concepts, not everyone reached the level of competence required to recover the cluster in the case of a failure. This experience taught me that not everyone can be an expert in everything simultaneously. Relying solely on shared knowledge and verbal communication proved insufficient. Documenting processes in a clear and accessible manner, in a way that is understandable for non-experts, is essential and also provides a valuable resource for onboarding new team members.

Since I started to study again, I transitioned into a part-time role, mostly giving input and taking on smaller tasks.

### Full Stack Engineer

 <badge text="Go" />
 <badge text="Elm" />
 <badge text="TS" />
 <badge text="PHP" />
 <badge text="Bash" />
 <badge text="Node.JS" />
 <badge text="Vue" />
 <badge text="Kubernetes" />
 <badge text="Docker" />
 <badge text="Nix" />
 <badge text="MongoDB" />
 <badge text="MySQL" />
 <badge text="REST" />
 <badge text="OpenAPI" />
 <badge text="Serverless" />
 <badge text="DevOps" />
 <badge text="CI/CD" />
 <badge text="Jira" />

- **Lead the transition from classic servers to Kubernetes.** \
  Setup core cluster functionality, like logging, monitoring, alerts and routing. Helped migrate all existing services, and designed and lead the move to a fully cloud-native architecture, achieving over **99.998% uptime** over the past 3 years while **reducing server costs by 45%** at the same time.
- **Introduced release workflows, code reviews, CI/CD, and reproducible dev and test environments.** \
  Gave internal presentations and wrote documentation about the architecture of our software, our processes, and our tech stack.
- **Mentored more junior members of the team.** \
  Showed them the basics of our workflow, Git, the Linux console, and Vue, while also introducing them to our codebase.
- Designed & implemented a uniform REST API for all of our services as well as a microservice-based import/export system, handling over 1 million vehicles per day. Built various other supporting services, while also contributing to the DMS as well.


### Web/App Developer

 <badge text="JS" />
 <badge text="TS" />
 <badge text="PHP" />
 <badge text="Java" />
 <badge text="Node.JS" />
 <badge text="MongoDB" />
 <badge text="MySQL" />
 <badge text="Vue" />
 <badge text="Quasar" />
 <badge text="Capacitor" />
 <badge text="Tailwind" />


- **Developed cross-platform AI-powered apps.** \
  Take photos of vehicles, automatically post-process them and extract essential information using AI, and upload everything to the cloud.
- **Built an extensible customer-facing showroom**, \
  offering a rich client-side search experience while being fully customizable for our customers. Helped integrate and customize in over 300 websites.
- Helped develop new features in our dealer management system, solving many backend and frontend challenges.

---

## Sysgrade GmbH <small text="2015-2018" />

I joined Sysgrade as a working student on order to not take on a loan while studying.
Basically by chance, I got to work on a very big project right ahead, which I still count among the work I'm most proud of to this day. While I was programming for a few years at this point as a hobby in high school, I've never worked on a project of this scale or with a team before. I've learned a lot about working in a professional environment, and the importance of clear communication and documentation.

### Software developer

 <badge text="C#" />
 <badge text="PHP" />
 <badge text="JS" />
 <badge text="VB.NET" />
 <badge text="MySQL" />
 <badge text="MSSQL" />
 <badge text="WPF" />
 <badge text="WCF" />
 <badge text="Shopware" />
 <badge text="Magento" />
 <badge text="Typo3" />
 <badge text="Symphony" />
 <badge text="jQuery"  />
 <badge text="Boostrap" />


- **Rearchitected and implemented a rewrite of our main import/export software**, \
  designing a modular, plugin-based architecture making heavy use of lazily-loaded streamed database queries, achieving up to a 100,000% performance improvements (yes, really!). Implemented plugins for various different ERP and shop systems, as well as customer-specific customizations.
- Improved the performance of Magento shops by up to 90% by introducing and configuring cache layers, and optimizing queries and assets.
- Helped implement plugins and themes for Shopware and Magento shops, as well as some Typo3 websites.

---

## Other skills

As I already mentioned, I love exploring new ideas and technologies. Some of them that stuck a little bit longer, but I haven't found an excuse to use professionally, are:

 <badge text="C/C++" />
 <badge text="Elm" />
 <badge text="Python" />
 <badge text="Haskell" />
 <badge text="F#" />
 <badge text="R" />
 <badge text="SvelteKit" />
 <badge text="React" />
 <badge text="Unity" />
 <badge text="Blender" />
 <badge text="GIMP" />

You might have noticed that I tend to be very good with computers in general, so of course I can also do the MS Office stuff as well, but I try actively not to.

## Eduction

#### B.Sc. AI and Data Science <small text="10/2022 - ongoing" />

OTH Regensburg

#### Media informatics <small text="2014 - 2018" />

University of Regensburg

#### High school <small text="2005 - 2013" />

Gymnasium der Regensburger Domspatzen

Diplomas and work references/recommendations are available on request.
