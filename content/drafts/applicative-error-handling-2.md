---
title: Composable error handling in Typescript
subtitle: Using functional techniques TODO TODO
description: TODO TODO
---

1. Programming is all about parsing
2. What do I mean by that?
2. Immutable Data enforces invariants by default
3. Exceptions?
4. Applicative I: Primitives,
5. Applicatives II: obj(), arr()
6. Applicatives III: Discovering the general structure, sequence, traverse, map2, etc.
7. One step further: `type Parser A B = (input: A) => Result B`
8. Library, Conclusion

Motivation: Programming in the small is all about parsing, exceptions don't compose, we can build something better

Example problem:
 - Seat reservation?



---

Programming in the small is all about data transformations. You get a request, you turn it into a response. Some input events happen, and you turn that into an updated game state. Maybe you take your command line arguments, and turn that into a series of syscalls for the runtime to perform.

> That last example might be suprising, but it's actually where the low-level interface is heading as well. Syscalls do have a significant overhead, so [modern kernels](https://man.archlinux.org/man/io_uring.7) [offer ways](https://learn.microsoft.com/en-us/windows/win32/fileio/i-o-completion-ports) to just write actions as data into a ring-buffer that gets periodically checked by the kernel, without the need to perform the syscall directly.

Unfortunately for us, these data transformations often can fail, or are only defined partially. Even something as simple as reading a verbosity level (an integer) from an environment variable might not work for a whole bunch of reasons:

 - The environment variable is not defined
 - The environment variable is defined, but is empty
 - The value is not a numeric string, or not in a format that is recognized
 - The log level is outside of the allowed range
 - It conflicts with other settings, like command line arguments

Even worse, as programmers we like to pretend that those things "can never happen", and that these are the unimportant cases, which can be safely ignored, forever hidden such that we only have to look at the happy case. Typescript also does not help us in those cases, and likes to pretend that errors can never happen.


I believe that just like programming is about data transformations, it is also about being aware of all those error cases, handling them appropriately. The case where errors can happen _is_ the common case, and pretending that it's unimportant or some kind of unlikely edge case condition TODO TODO TODO


So programming is about data transformations that might fail.

```ts
type Result<T> =
    | { isOk: true, value: T }
    | { isOk: false, errors: Error[] }

function ok<T>(value: T): Result<T> {
    return { isOk: true, value }
}

function err(...errors: Error[]): Result<never> {
    return { isOk: false, errors }
}

function extract<T>(result: Result<T>, defaultValue: T): T {
    if (result.isOk) {
        return result.value
    } else {
        return defaultValue
    }
}
```

```ts

type Parser<A, B> = (input: A) => Result<B>

function fromFunction<A, B>(f: (input: A) => B): Parser<A, B> {
    return (input) => ok(f(input))
}

function fromValue<T>(value: T) => Parser<never, T> {
    const result = ok(value)
    return _ => result
}

function both<Input, A, B, Result>(parser1: Parser<Input, A>, parser2: Parser<Input, B>, combine: (a: A, b: B) => Result): Parser<Input, Result> {
    return input => map2(parser1(input), parse2(input), combine)
}

function sequence<A, B, C>(parser1: Parser<A, B>, parser2: Parser<B, C>): Parser<A, C> {
    return input => chain(parser1(input), parser2)
}

interface Parsers<Input, T> {
    [Key in keyof T]: Parser<Input, T[Key]>
}

function objParser<Input, T>(parsers: Parsers<Input, T>): Parser<Input, T> {
    return function (input) {
        // obj(mapObj(parsers, parser => parser(input)))
        // traverse :: Parser T R -> Parser T[] R[]
        // traverseObj(parsers, parser => parser(input)) // doesn't typecheck (higher kinded types; the traverse function is forall a b.)

        let result = ok<T>({} as unknown as T)
        for (const key in parsers) {
            if (parsers.hasOwnProperty(key)) {
                result = map2(result, results[key](input), (soFar, val: T[typeof key]) => ({ ...soFar, [key]: val }))
            }
        }
        return result
    }
}
```


```ts
function map<A, B>(result: Result<A>, f: (value: A) => B): Result<B> {
    if (result.isOk) {
        return ok(f(result.value))
    } else {
        return result
    }
}

function chain<A, B>(result: Result<A>, f: (value: A) => Result<B>): Result<B> {
    if (result.isOk) {
        return f(result.value)
    } else {
        return result
    }
}

function map2<A, B, R>(aR: Result<A>, bR: Result<B>, combine: (a: A, b: B) => R): Result<R> {
    if (aR.isOk && bR.isOk) {
        return ok(combine(aR.value, bR.value))
    } else if (!aR.isOk && !bR.isOk) {
        return err(...aR.errors, ...bR.errors)
    } else if (aR.isOk && !bR.isOk) {
        return bR
    } else if (!aR.isOk && bR.isOk) {
        return aR
    }
    // TODO: Can we get rid of this?
    throw "unreachable"
}
```

```ts
function isString(x: any): x is string {
    return typeof x === 'string'
}

function guard<T>(guardF: (x: any) => x is T, val: any): Result<T> {
    if (guardF(val)) {
        return ok(val)
    } else {
        return err(new TypeError(`Type guard ${guardF.name} not satisfied`))
    }
}
```


```ts
function tryCatch<Args extends any[], T>(fn: (...args: Args) => T): (...args: Args) => Result<T> {
    return function tryCatch (...args: Args): Result<T> {
        try {
            return ok(fn(...args))
        } catch (thrownValue) {
            if (thrownValue instanceof Error) {
                return err(thrownValue)
            } else {
                return err(new Error(`${thrownValue}`))
            }
        }
    }
}
```

```ts
function onError<T>(result: Result<T>, onError: (errors: Error[]) => Result<T>): Result<T> {
    if (!result.isOk) {
        return onError(result.errors)
    } else {
        return result
    }
}
```


```ts
type Results<T> = {
    [Key in keyof T]: Result<T[Key]>
}

function list<T extends readonly any[]>(results: Results<T>): Result<T> {
    return results.reduce(
        (state, value) => map2(state, value, (soFar: T, val: unknown) => [...soFar, val]),
        ok<T>([] as unknown as T)
    )
}

function obj<T extends object>(results: Results<T>): Result<T> {
    let result = ok<T>({} as unknown as T)
    for (const key in results) {
        if (results.hasOwnProperty(key)) {
            result = map2(result, results[key], (soFar, val: T[typeof key]) => ({ ...soFar, [key]: val }))
        }
    }
    return result
}
```

```ts
function mapN<Args extends readonly any[], T>(fn: (...args: Args) => T, ...args: Results<Args>): Result<T> {
    return map(list(args), args => fn(...args))
}

function chainN<Args extends readonly any[], T>(fn: (...args: Args) => Result<T>, ...args: Results<Args>): Result<T> {
    return chain(list(args), args => fn(...args))
}

function first<T>(...results: Array<Result<T>>): Result<T> {
    // chain(list(results), arr => get(arr, 0))
    return map(list(results), arr => arr[0]!)
}

function fold<T, R>(result: Result<T>, onOk: (value: T) => R, onErr: (errors: Error[]) => R): R {
    if (result.isOk) {
        return onOk(result.value)
    } else {
        return onErr(result.errors)
    }
}
```


## References

- King A. (2019). [Parse, don't validate](https://lexi-lambda.github.io/blog/2019/11/05/parse-don-t-validate/)
