---
title: Composable error handling in Typescript
subtitle: Using functional techniques TODO TODO
description: TODO TODO
---

- Programming is all about data transformations
    - Data transformations are everywhere, even syscalls (io_uring)
    - It makes things nicer if those are pure
        - easier to predict and debug
        - trivial to test
    - ... but we need to do side-effects to have a useful program
- Parsing -> Business Logic -> Serialization
    - Looking one step closer, this is most often the structure
    - Side-effects mainly happen at the edges
    - ( Effect types make that obvious, free monad if interleaved effects? )
    - Effects always can fail, data from the outside world may not be what we expect
- We want to protect our Domain, only valid data
    - shit-in != shit-out
    - Our programs should have well defined outputs for all inputs
    - So it's actually all about the edge cases
    - We need to validate all data that gets into our program
    - This is part of our behaviour and we should be explicit about it or at least have thought about what would happen
- Types as proofs (of validation), encapsulation, constructors that can fail
    - If we make the type constructor private, we can ensure that all values are constructed by our module
    - The module can make sure it always does the validation
    - Having a value of a type can therefore be a proof that we validated it
    - So it's not about validating at all, it's more like parsing!
    - If our values are immutable, we can't break our invariants
    - If all our values are immutable, this works even when nesting references
    - setters also mean we have to either have a
        - valid "empty" state
        - duplicate all validation logic
- How we might do this "traditionally"
    - Exceptions
        - Exceptions don't compose
        - we only see look at the happy path
        - type signatures lie to us
        - there is no way of knowing if/how something might fail
        - we only ever get the first failure ("depth-first search")
        - it's really hard to add contextual information
    - Specialized validation libraries, zod, joi
        - only work for that exact problem
        - validate, don't parse
        - are uneccesarily complicated compared to what we are about to do
- applicative results
    - What if we just dealt with things as values and then wrote data transformations?
    - ...
    - map2 -> traverse is pretty awesome and solves all our problems and is like 20loc
    - We can make private types by using branded types
    - Profit
- summary
    - We have a pure domain separated from the outside world (ports and adapters?)
    - Once we are in our business logic code, we can be confident that values are valid
    - We can easily test stuff in our domain
    - We know exactly what can fail and how
    - We have a composable way of writing those functions


2. What do I mean by that?
3. Exceptions?
4. Applicative I: Primitives,
5. Applicatives II: obj(), arr()
6. Applicatives III: Discovering the general structure, sequence, traverse, map2, etc.
7. Library, Conclusion

Motivation: Programming in the small is all about parsing, exceptions don't compose, we can build something better

Example problem:
 - Seat reservation?



---

Programming in the small is all about data transformations. You get a request, you turn it into a response. Some input events happen, and you turn that into an updated game state. Maybe you take your command line arguments, and turn that into a series of syscalls for the runtime to perform.

> That last example might be suprising, but it's actually where the low-level interface is heading as well. Syscalls do have a significant overhead, so [modern kernels](https://man.archlinux.org/man/io_uring.7) [offer ways](https://learn.microsoft.com/en-us/windows/win32/fileio/i-o-completion-ports) to just write actions as data into a ring-buffer that gets periodically checked by the kernel, without the need to perform the syscall directly.

Unfortunately for us, these data transformations often can fail, or are only defined partially. Even something as simple as reading a verbosity level (an integer) from an environment variable might not work for a whole bunch of reasons:

 - The environment variable is not defined
 - The environment variable is defined, but is empty
 - The value is not a numeric string, or not in a format that is recognized
 - The log level is outside of the allowed range
 - It conflicts with other settings, like command line arguments

Even worse, as programmers we like to pretend that those things "can never happen", and that these are the unimportant cases, which can be safely ignored, forever hidden such that we only have to look at the happy case. Typescript also does not help us in those cases, and likes to pretend that errors can never happen.


I believe that just like programming is about data transformations, it is also about being aware of all those error cases, handling them appropriately. The case where errors can happen _is_ the common case, and pretending that it's unimportant or some kind of unlikely edge case condition TODO TODO TODO


So programming is about data transformations that might fail.



## References

- King A. (2019). [Parse, don't validate](https://lexi-lambda.github.io/blog/2019/11/05/parse-don-t-validate/)
