const domParser = new DOMParser()
async function htmx(elt, method) {
  // fallback to the current URL if no hx-[verb] attribute is given
  const url = elt.getAttribute(`hx-${method}`) ?? location.href

  const swapSpec = getSwap(elt)
  const target = getTarget(elt)
  if (!target) {
    return // no target, nothing to do
  }

  const body = method !== 'get'
    ? getBody(elt)
    : undefined

  const request = await fetch(url, { method, body })
  if (!request.ok || request.status === 204) {
    return // don't swap on NO CONTENT or error
  }

  const html = await request.text()
  const doc = domParser.parseFromString(`<body><template>${html}</template></body>`, 'text/html')
  const content = doc.querySelector('template').content

  swap(swapSpec, target, content)
}

function getBody(elt) {
  // TODO: collect form data, hx-params, hx-vars, hx-include, etc.
  return undefined
}

function getTarget(elt) {
  // TODO: support hx-target
  return elt
}

function getSwap(elt) {
  // TODO: hx-swap
  return undefined
}

function swap(spec, target, content) {
  deinit(target)
  // init first, because replaceWith moves our elements to the other document
  init(...content.children)
  target.replaceWith(content)
}

// a property on the DOM nodes, holding our state
const stateProp = Symbol()

// trigger is our event handler function
const verbs = ['get', 'post', 'put', 'patch', 'delete']
function trigger(evt, spec) {
  const elt = evt.currentTarget
  // I'm not sure what a useful way to support multiple hx-[verb]
  // attributes might look like...
  const verb = verbs.find(verb => elt.hasAttribute(`hx-${verb}`))
  if (!verb) {
    return //hx-[verb] attribute got removed, can't do nothing
  }

  // TODO: hx-sync, hx-confirm, hx-push-url, ...
  evt.preventDefault()
  htmx(elt, verb)
}

// connect initializes all event handlers on an element
function connect(elt) {
  if (elt[stateProp]) {
    return // already initialized
  }

  // keep a list of cleanup/finalize/dispose callbacks to run on disconnect
  const state = elt[stateProp] = { cleanup: [] }

  const triggers = parseSpecAttribute(elt, 'hx-trigger')
  // no triggers found, add the "natural trigger" instead
  if (!triggers.length) {
    triggers.push(getNaturalTrigger(elt))
  }

  for (const spec of triggers) {
    const eventName = spec.value
    const handler = evt => trigger(evt, spec)
    elt.addEventListener(eventName, handler)
    state.cleanup.push(() => elt.removeEventListener(eventName, handler))
  }
}

// parse a spec attribute like hx-trigger or hx-swap
function parseSpecAttribute(elt, attributeName) {
  // TODO: inheritance
  const str = elt.getAttribute(attributeName)
  if (!str) {
    return []
  }

  // TODO: multiple specs, modifiers
  return [{ value: str.trim() }]
}

// get the spec for the natural event of the element
function getNaturalTrigger(elt) {
  if (elt.matches('input:not([type=submit],[type=button]),textarea,select')) {
    return { value: 'change' }
  } else if (elt.matches('form')) {
    return { value: 'submit' }
  } else {
    return { value: 'click' }
  }
}

function disconnect(elt) {
  const state = elt[stateProp]
  if (!state) {
    return // no instance, disconnect called twice
  }

  elt[stateProp] = null

  for (const cleanup of state.cleanup) {
    cleanup()
  }
}

// we need to connect an element if it has any hx-[verb] attribute
const verbSelector = verbs.map(verb => `[hx-${verb}]`).join(',')

// query all children, and also this element if it matches
function* queryAllAndSelf(selector, ...elts) {
  for (const elt of elts) {
    if (elt.nodeType !== Node.ELEMENT_NODE) {
      continue
    }

    if (elt.matches(selector)) {
      yield elt
    }

    yield* elt.querySelectorAll(selector)
  }
}

// call connect on all htmx elements in a subtree
function init(...elts) {
  for (const elt of queryAllAndSelf(verbSelector, ...elts)) {
    connect(elt)
  }
}

// cleanup all htmx event listeners in a subtree
function deinit(...elts) {
  for (const elt of queryAllAndSelf(verbSelector, ...elts)) {
    disconnect(elt)
  }
}

// on startup, init the entire document.
if (document.readyState !== 'loading') {
  init(document.body)
} else {
  document.addEventListener('DOMContentLoaded', () => init(document.body))
}

