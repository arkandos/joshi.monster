const domParser = new DOMParser()
async function htmx(elt, method) {
  // just use the elt as a fallback, so we always have an indicator
  const indicator = hxQuerySelector(elt.getAttribute('hx-indicator'), elt) ?? elt
  indicator.classList.add('htmx-request')
  try {
    // fallback to the current URL if no hx-[verb] attribute is given
    const url = elt.getAttribute(`hx-${method}`) ?? location.href

    const swapSpec = getSwap(elt)
    const target = getTarget(elt)
    if (!target) {
      return // no target, nothing to do
    }

    const body = method !== 'get'
      ? getBody(elt)
      : undefined

    const request = await fetch(url, { method, body })
    if (!request.ok || request.status === 204) {
      return // don't swap on NO CONTENT or error
    }

    const html = await request.text()
    const doc = domParser.parseFromString(`<body><template>${html}</template></body>`, 'text/html')
    const content = doc.querySelector('template').content

    swap(swapSpec, target, content)
  } finally {
    indicator.classList.remove('htmx-request')
  }
}

function getSwap(elt) {
  const specs = parseSpecAttribute(elt, 'hx-swap')
  return specs[0]
}

function getBody(elt) {
  // TODO: hx-include, hx-params, hx-vals, hx-validate, ...
  const form = elt.closest('form')
  if (form) {
    // use the browser to include a surrounding form
    return new URLSearchParams(new FormData(form))
  } else if (elt.name) {
    // always include the triggering element, if it has a name
    return new URLSearchParams({ [elt.name]: elt.value })
  } else {
    // no form, no name on the elt
    return undefined
  }
}

function eatPrefix(str, prefix) {
  if (str.startsWith(prefix)) {
    return str.substr(prefix.length)
  } else {
    return false
  }
}

function hxQuerySelector(selector, context) {
  let suffix
  if (!selector || selector === 'this') {
    // TODO: inheritance
    return context
  // special keywords
  } else if (selector === 'window') {
    return window
  } else if (selector === 'document') {
    return document
  } else if (selector === 'next') {
    return context.nextElementSibling
  } else if (selector === 'previous') {
    return context.previousElementSibling
  // prefix-based selectors
  } else if ((suffix = eatPrefix(selector, 'closest '))) {
    return context.closest(suffix)
  } else if ((suffix = eatPrefix(selector, 'find '))) {
    return context.querySelector(suffix)
  } else if ((suffix = eatPrefix(selector, 'next '))) {
    return Array.from(document.querySelectorAll(suffix))
      .find(node => context.compareDocumentPosition(node)
        === Node.DOCUMENT_POSITION_FOLLOWING)
  } else if ((suffix = eatPrefix(selector, 'previous '))) {
    return Array.from(document.querySelectorAll(suffix))
      .findLast(node => context.compareDocumentPosition(node)
        === Node.DOCUMENT_POSITION_PRECEDING)
  } else {
    return document.querySelector(selector)
  }
}

function getTarget(elt) {
  return hxQuerySelector(elt.getAttribute('hx-target'), elt)
}

function swap(spec, target, content) {  
  const initContent = () => init(...content.children)

  const swapStyle = spec?.value ?? 'innerHTML'
  console.log('swap', swapStyle, target, content)
  if (swapStyle === 'innerHTML') {
    deinit(...target.children)
    initContent()
    target.replaceChildren(content)
  } else if (swapStyle === 'outerHTML') {
    deinit(target)
    initContent()
    target.replaceWith(content)
  } else if (swapStyle === 'delete') {
    deinit(target)
  } else if (swapStyle !== 'none') {
    initContent()
    target.insertAdjacentElement(swapStyle, content)
  }
}

// a property on the DOM nodes, holding our state
const stateProp = Symbol()

// trigger is our event handler function
const verbs = ['get', 'post', 'put', 'patch', 'delete']
function trigger(evt, spec) {
  const elt = evt.currentTarget
  const state = elt[stateProp]
  if (!state) {
    return // not connected - should not happen?
  }

  const verb = verbs.find(verb => elt.hasAttribute(`hx-${verb}`))
  if (!verb) {
    return //hx-[verb] attribute got removed, can't do nothing
  }

  // TODO: hx-sync, hx-confirm, hx-push-url, ...
  evt.preventDefault()

  if (spec.delay) {
    // start a timeout if delay is active
    if (state.timeout) {
      clearTimeout(state.timeout)
    }

    state.timeout = setTimeout(handle, spec.delay)
  } else {
    // no delay, dispatch directly
    handle()
  }

  function handle() {
    if (spec.changed && state.lastValue === elt.value) {
      return
    }

    state.lastValue = elt.value

    htmx(elt, verb)
  }
}

// connect initializes all event handlers on an element
function connect(elt) {
  if (elt[stateProp]) {
    return // already initialized
  }

  // keep a list of cleanup/finalize/dispose callbacks to run on disconnect
  const state = elt[stateProp] = { cleanup: [] }

  const triggers = parseSpecAttribute(elt, 'hx-trigger', {
    delay: parseInterval
  })
  // no triggers found, add the "natural trigger" instead
  if (!triggers.length) {
    triggers.push(getNaturalTrigger(elt))
  }

  for (const spec of triggers) {
    const eventName = spec.value
    const handler = evt => trigger(evt, spec)
    elt.addEventListener(eventName, handler)
    state.cleanup.push(() => elt.removeEventListener(eventName, handler))
  }
}

// parse a spec attribute like hx-trigger or hx-swap
function parseSpecAttribute(elt, attributeName, modifierParsers = {}) {
  // TODO: inheritance
  const str = elt.getAttribute(attributeName)
  if (!str) {
    return []
  }

  const result = []
  // split on comma, and also remove surrounding spaces
  for (const specStr of str.split(/\s*,\s*/)) {
    if (!specStr) {
      continue // skip empty parts
    }

    // first "word" is the main value, rest is modifiers
    const [value, ...modifiers] = specStr.split(/\s+/)
    
    const spec = { value }
    for (const modifier of modifiers) {
      const comma = modifier.indexOf(':')
      if (comma >= 0) {
        // modifier with a value
        const modifierKey = modifier.substr(0, comma)
        const modifierValue = modifier.substr(comma+1)
        const modifierParser = modifierParsers[modifierKey]
        if (modifierParser) {
          // modifier with values in a special format, like `delay`
          spec[modifierKey] = modifierParser(modifierValue)
        } else {
          // simple modifiers with values
          spec[modifierKey] = modifierValue
        }
      } else {
        spec[modifier] = true // toggle modifier, like changed
      }
    }

    result.push(spec)
  }

  return result
}

function parseInterval(str) {
  if (str.endsWith('ms')) {
    return parseFloat(str)
  } else if (str.endsWith('s')) {
    return parseFloat(str) * 1000
  } else if (str.endsWith('m')) {
    return parseFloat(str) * 60 * 1000
  } else {
    return parseFloat(str)
  }
}

// get the spec for the natural event of the element
function getNaturalTrigger(elt) {
  if (elt.matches('input:not([type=submit],[type=button]),textarea,select')) {
    return { value: 'change' }
  } else if (elt.matches('form')) {
    return { value: 'submit' }
  } else {
    return { value: 'click' }
  }
}

function disconnect(elt) {
  const state = elt[stateProp]
  if (!state) {
    return // no instance, disconnect called twice
  }

  elt[stateProp] = null

  for (const cleanup of state.cleanup) {
    cleanup()
  }

  if (state.timeout) {
    clearTimeout(state.timeout)
  }
}

// we need to connect an element if it has any hx-[verb] attribute
const verbSelector = verbs.map(verb => `[hx-${verb}]`).join(',')

// query all children, and also this element if it matches
function* queryAllAndSelf(selector, ...elts) {
  for (const elt of elts) {
    if (elt.nodeType !== Node.ELEMENT_NODE) {
      continue
    }

    if (elt.matches(selector)) {
      yield elt
    }

    yield* elt.querySelectorAll(selector)
  }
}

// call connect on all htmx elements in a subtree
function init(...elts) {
  for (const elt of queryAllAndSelf(verbSelector, ...elts)) {
    connect(elt)
  }
}

// cleanup all htmx event listeners in a subtree
function deinit(...elts) {
  for (const elt of queryAllAndSelf(verbSelector, ...elts)) {
    disconnect(elt)
  }
}

// on startup, init the entire document.
if (document.readyState !== 'loading') {
  init(document.body)
} else {
  document.addEventListener('DOMContentLoaded', () => init(document.body))
}

