# Oct 16, 2023

- [x] [A serverless proxy (ab)using a service workers and WebRTC](https://proxy.joshi.monster/)

# Sep 15, 2023

- [I packaged my WebRTC helpers, but did not write docs yet](https://www.npmjs.com/package/@jreusch/webrtc)
- [A single-function package that works like p-limit, but also gives you exclusive access to a shared resource](https://www.npmjs.com/package/@jreusch/p-worker)
- [Another single-function package that allows you to directly access a Promises internals](https://www.npmjs.com/package/@jreusch/p-destruct)

# Sep 1, 2023

- [x] [Build a webserver specifically for dev, giving you automatic project.localhost domains](https://gitlab.com/arkandos/localhostd)

# Jul 31, 2023

- [x] [Added a server-side module to my routing library that works with everything](https://www.npmjs.com/package/@jreusch/router-node)

# Jun 2023

- [A birdsite clone using SvelteKit for university](https://gitlab.com/arkandos/sose2023-wt)

# Mar 2023

- [git-gud: A bash script that uses ChatGPT to flame you for your commits](https://gitlab.com/arkandos/git-gud)
- [Bash script to rename a branch while also using Gitlab/Githubs APIs to make sure permissions are correct](https://gitlab.com/arkandos/git-rename-branch)
- [CRLF to LF, not invented here](https://gitlab.com/arkandos/uncrlfuck)

# Feb 3, 2023

- [Use the WebRTC API to figure out your IP](https://ip.joshi.monster/)
- [Test your webcam and mic, without having to open some sketchy page](https://cam.joshi.monster/)

# Jan 2023

- [Experimented with TensorFlow to make it solve TSP problems](https://gitlab.com/arkandos/tspnn)

# Dec 25, 2022

- [A browser plugin that embeds toots directly if a link to another toot is found](https://gitlab.com/arkandos/quote-toots)

# Nov 2022

- [A generic typescript routing library with bindings for React and Vue](https://gitlab.com/arkandos/ts-router)

# Oct 2022

- [Share files using WebRTC](https://share.joshi.monster/)

# Jan 2020

- [An exploration on how to do SSR in Elm (just use elm-pages instead!)](https://gitlab.com/arkandos/elm-ssr-exploration)
- [A PWA built with Elm to track what you've had to drink](https://gitlab.com/arkandos/sax)

# Oct 2018

- [A software raytracer in Java (university project)](https://gitlab.com/arkandos/software_engineering-raytracer)
- [Game of Life in Java (university project)](https://gitlab.com/arkandos/software_engineering-gameoflife)

# Nov 2018

- [A classical file-sharing app in Elm using CouchDB for storage](https://gitlab.com/arkandos/shareit)

# Sep 2018

- [PM for an App for an upcoming event at my city (university project)](https://gitlab.com/arkandos/stadt-land-fluss)

# Sep 2016

- [A multiplayer online Durak game (university project)](https://gitlab.com/arkandos/durak)

# Sep 2015

- [A space-themed Flappy Bird clone (university project)](https://gitlab.com/arkandos/spacebirds)

