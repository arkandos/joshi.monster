import { codeToTokens } from 'shiki'

export async function highlightCode({ code, lang, theme }) {
    return codeToTokens(code, { lang, theme })
}
