import { parseDocument, DomUtils } from 'htmlparser2'

// NOTE: not using netlify's API because it doesnt even work inside their functions!
// I always got an error that says `Cannot find module '@netlify/open-api'`
// import { NetlifyAPI } from 'netlify'
import { createRestAPIClient as createMastodonClient } from 'masto'

const BRANCH = 'main'
const RSS_URL = '/rss.xml'

export default async function handler(req, context) {
    const NETLIFY_ACCESS_TOKEN = Netlify.env.get('NETLIFY_ACCESS_TOKEN')
    const MASTODON_INSTANCE = Netlify.env.get('MASTODON_INSTANCE')
    const MASTODON_ACCESS_TOKEN = Netlify.env.get('MASTODON_ACCESS_TOKEN')

    if (!NETLIFY_ACCESS_TOKEN || !MASTODON_INSTANCE || !MASTODON_ACCESS_TOKEN) {
        throw new Error('required environment variables missing.')
    }

    // const netlify = new NetlifyAPI(NETLIFY_ACCESS_TOKEN, {})

    // note: we need the following permissions:
    //  - write:media
    //  - write:statuses
    // we don't use write:media at the moment, but we might want to in the future!

    const mastodon = createMastodonClient({
        url: MASTODON_INSTANCE,
        accessToken: MASTODON_ACCESS_TOKEN
    })

    // const deploys = await netlify.listSiteDeploys({
    //     siteId: context.site.id,
    //     // only sites that we can reach
    //     state: 'ready',
    //     // only production deploys (going to the main branch)
    //     production: true,
    //     branch: BRANCH,
    //     // we need the latest and the previous.
    //     perPage: 2,
    //     latestPublished: true
    // })

    // NOTE: you CAN access the required data without a access token!
    // you just have to "guess" the permalink a little bit!
    // instead of getting .links.permalink directly, it looks like the permalink
    // always follows this pattern: `https://${.build_id}--${.name}.netlify.app`

    const url = new URL(`https://api.netlify.com/api/v1/sites/${context.site.id}/deploys`)
    url.searchParams.append('state', 'ready')
    url.searchParams.append('production', 'true')
    url.searchParams.append('branch', BRANCH)
    url.searchParams.append('per_page', '2')
    url.searchParams.append('latest-published', 'true')

    console.log('Loading deploys from', url.toString())
    const deploys = await fetch(url, {
        headers: {
            Authorization: 'Bearer ' + NETLIFY_ACCESS_TOKEN
        }
    }).then(res => res.json())

    const newFeedUrl = new URL(RSS_URL, deploys[0].links.permalink)
    const oldFeedUrl = new URL(RSS_URL, deploys[1].links.permalink)

    console.log('new feed url:', newFeedUrl.toString())
    console.log('old feed url:', oldFeedUrl.toString())

    const [
        newFeed,
        oldFeed
    ] = await Promise.all([
        loadFeed(newFeedUrl),
        loadFeed(oldFeedUrl)
    ])

    const itemsToPublish = diffItems(oldFeed, newFeed)
    if (!itemsToPublish.length) {
        console.log('no new items found.')
        return new Response('', { status: 201 })
    }

    // we know we have at least one item in itemsToPublish.
    console.log('publishing', itemsToPublish.length, 'new items')

    // we want to publish in reverse order, such that the oldest new item becomes
    // the first status we post, such that at the end, everything lines up again
    itemsToPublish.sort((a, b) => a.pubDate - b.pubDate)

    for (const item of itemsToPublish) {
        // we rely on the fact that Mastodon shows a link preview, that way we
        // don't need to include the title at all.
        // If we have no description, still set it though, just to have something in the post
        // Mastodon automatically parses raw text statuses on the server.
        let statusText = `${item.description || item.title}\n\n${item.link}`
        if (item.categories.length) {
            statusText += '\n\n' + item.categories
                .map(cat => '#' + cat)
                .join(' ')
        }

        console.log('posting', item.title, '(', item.link, ')')

        const status = await mastodon.v1.statuses.create({
            visibility: 'public',
            status: statusText,
        })

        console.log(' --> new status: ', status.url)
    }

    return new Response('', { status: 200 })
}


function diffItems(oldFeed, newFeed) {
    const newItemsById = new Map()
    for (const item of newFeed.items) {
        newItemsById.set(cleanLink(item.link), item)
    }
    for (const item of oldFeed.items) {
        newItemsById.delete(cleanLink(item.link))
    }

    return [...newItemsById.values()]
}

function cleanLink(link) {
    // I wonder why I do this??
    return link.replaceAll(/([^:])\/\//g, '$1/')
}

async function loadFeed(rssUrl) {
    const res = await fetch(rssUrl)
    const rss = await res.text()

    const doc = parseDocument(rss, { xmlMode: true })

    // see https://github.com/fb55/domutils/blob/master/src/feeds.ts#L155
    // we copy this function because we need to load categories as well.
    const root = DomUtils.getElementsByTagName('rss', doc.children, true, 1)[0]
    const channel = DomUtils.getElementsByTagName('channel', root.children, true, 1)[0]?.children ?? []
    const items = DomUtils.getElementsByTagName('item', root.children)

    const getText = (tagName, where, recurse = false) =>
        DomUtils.textContent(DomUtils.getElementsByTagName(tagName, where, recurse, 1)).trim() || undefined

    const getDate = (tagName, where) => {
        const dateStr = getText(tagName, where)
        if (!dateStr) {
            return null
        }

        return new Date(dateStr)
    }

    const feed = {
        type: 'rss',
        id: '',

        title: getText('title', channel),
        link: getText('link', channel),
        description: getText('description', channel),
        updated: getDate('lastBuildDate', channel),
        author: getText('managingEditor', channel, true),

        items: items.map(item => ({
            id: getText('guid', item.children),
            title: getText('title', item.children),
            link: getText('link', item.children),
            description: getText('description', item.children),
            pubDate: getDate('pubDate', item.children) || getDate('dc:date', item.children),

            media: DomUtils.getElementsByTagName('media:content', item.children).map(mediaEl => {
                const { attribs } = mediaEl

                const media = {
                    medium: attribs.medium,
                    expression: attribs.expression,
                    isDefault: !!attribs.isDefault,
                }

                for (const key of ["url", "type", "lang"]) {
                    if (attribs[key]) {
                        media[key] = attribs[key]
                    }
                }

                for (const key of ["fileSize", "bitrate", "framerate", "samplingrate", "channels", "duration", "height", "width"]) {
                    if (attribs[key]) {
                        media[key] = parseInt(attribs[key], 10)
                    }
                }

                return media
            }),

            categories: DomUtils.getElementsByTagName('category', item.children).map(categoryEl =>
                DomUtils.textContent(categoryEl).trim())
        }))
    }

    return feed
}
