module Api exposing (routes)

import ApiRoute exposing (ApiRoute)
import BackendTask exposing (BackendTask)
import Config
import Content
import Date
import FatalError exposing (FatalError)
import Html exposing (Html)
import Json.Encode as Encode
import List.Extra as List
import Pages
import Pages.Manifest as Manifest
import Route exposing (Route)
import Rss
import Server.Response


activityPublicKey =
    """
    -----BEGIN PUBLIC KEY-----
    MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwLeQ4glIhRUqDRF8SZFK
    B7HYA42K7p9DXtEOhooZn2usT3DLuV18SIvpO7dLIu4bUCeBzAd+hgV0EbG31FGH
    BP+vDKtlPLbRYZiOwDLy6ZaTVMVZaOqBhZplJFqUy5rYaI6ThlDJN/EvmlroNv1m
    9V2ALWpBqSZ8lWCfD5B0WSo5ajmJz/MLrgeG6YuLejOlq68SrxIQ9pHIvXpPgSo3
    up7YzypdIc0mRbBlF5J/hPkJ+c7WfTttfeC/gr8HYlEivviXKshVHAt7u49ucuZT
    quy74mDeLLroyOxQZ8w0dBl3DZK3Z/2xUB2yZg3Na6JO7AAIJWRnLU7QeAXPQTYa
    vwIDAQAB
    -----END PUBLIC KEY-----
    """


routes :
    BackendTask FatalError (List Route)
    -> (Maybe { indent : Int, newLines : Bool } -> Html Never -> String)
    -> List (ApiRoute ApiRoute.Response)
routes getStaticRoutes htmlToString =
    [ -- ActivityPub
      webfingerRoute
    , actorRoute

    -- , inboxRoute
    , outboxRoute
    , BackendTask.succeed manifest
        |> Manifest.generator Config.canonicalUrl

    -- sitemap
    , sitemap getStaticRoutes

    -- rss
    , rssFeed
    ]


sitemap : BackendTask FatalError (List Route) -> ApiRoute ApiRoute.Response
sitemap getStaticRoutes =
    let
        routeToSitemapEntry route =
            Config.canonicalUrl ++ Route.toString route
    in
    getStaticRoutes
        |> BackendTask.map (List.map routeToSitemapEntry >> String.join "\n")
        |> ApiRoute.succeed
        |> ApiRoute.literal "sitemap.txt"
        |> ApiRoute.single


rssFeed : ApiRoute ApiRoute.Response
rssFeed =
    Content.listPostMetadata
        |> BackendTask.map
            (\articles ->
                Rss.generate
                    { description = Config.description
                    , generator = Just Config.name
                    , lastBuildTime = Pages.builtAt
                    , siteUrl = Config.canonicalUrl
                    , title = Config.name
                    , url = Config.canonicalUrl
                    , items =
                        articles
                            |> List.sortWith
                                (\post1 post2 ->
                                    Date.compare post2.published post1.published
                                )
                            |> List.map
                                (\article ->
                                    { title = article.title
                                    , description = article.summary
                                    , url = Content.url article.slug
                                    , categories = article.tags
                                    , author = Config.fullName
                                    , pubDate = article.published
                                    , content = Nothing
                                    , contentEncoded = Nothing
                                    , enclosure = Nothing
                                    }
                                )
                    }
            )
        |> ApiRoute.succeed
        |> ApiRoute.literal "rss.xml"
        |> ApiRoute.single


manifest : Manifest.Config
manifest =
    Manifest.init
        { name = Config.name
        , description = Config.description
        , startUrl = Route.toPath Route.Index
        , icons = []
        }


webfingerRoute : ApiRoute ApiRoute.Response
webfingerRoute =
    ApiRoute.succeed
        (\_ ->
            Encode.object
                [ ( "subject", Encode.string "acct:me@joshi.monster" )
                , ( "aliases", Encode.list Encode.string [ "https://joshi.monster/.activitypub/actor" ] )
                , ( "links"
                  , Encode.list identity
                        [ Encode.object
                            [ ( "rel", Encode.string "self" )
                            , ( "type", Encode.string "application/activity+json" )
                            , ( "href", Encode.string "https://joshi.monster/.activitypub/actor" )
                            ]
                        ]
                  )
                ]
                |> Encode.encode 0
                |> Server.Response.body
                |> Server.Response.withHeader "X-Custom-Test" "helloworld"
                |> Server.Response.withHeader "Content-Type" "application/jrd+json"
                |> Server.Response.withStatusCode 418
                |> BackendTask.succeed
        )
        |> ApiRoute.literal ".well-known"
        |> ApiRoute.slash
        |> ApiRoute.literal "webfinger"
        |> ApiRoute.serverRender


actorRoute : ApiRoute ApiRoute.Response
actorRoute =
    ApiRoute.succeed
        (\_ ->
            Encode.object
                [ ( "@context"
                  , Encode.list identity
                        [ Encode.string "https://www.w3.org/ns/activitystreams"
                        , Encode.string "https://w3id.org/security/v1"
                        , Encode.object [ ( "@language", Encode.string "en-GB" ) ]
                        ]
                  )
                , ( "type", Encode.string "Application" )
                , ( "id", Encode.string "https://joshi.monster/.activitypub/actor" )

                -- these fields are required by ActivityStreams:
                , ( "name", Encode.string Config.name )

                -- mastodon will construct an acct: URI using that, which has to match
                , ( "preferredUsername", Encode.string "me" )

                -- , ( "inbox", Encode.string "https://joshi.monster/.activitypub/inbox" )
                , ( "outbox", Encode.string "https://joshi.monster/.activitypub/outbox" )
                , ( "publicKey"
                  , Encode.object
                        [ ( "id", Encode.string "https://joshi.monster/.activitypub/actor#main-key" )
                        , ( "owner", Encode.string "https://joshi.monster/.activitypub/actor" )
                        , ( "publicKeyPem", Encode.string activityPublicKey )
                        ]
                  )

                -- these fields are optional:
                , ( "summary", Encode.string <| Config.description ++ "\n\n - WORK IN PROGRESS, DO NOT INTERACT! (ask @jreusch@layer8.space if you have questions)" )
                ]
                |> Encode.encode 0
                |> Server.Response.body
                |> Server.Response.withHeader "Content-Type" "application/activity+json"
                |> BackendTask.succeed
        )
        |> ApiRoute.literal ".activitypub"
        |> ApiRoute.slash
        |> ApiRoute.literal "actor"
        |> ApiRoute.serverRender


outboxRoute : ApiRoute ApiRoute.Response
outboxRoute =
    ApiRoute.succeed
        (\_ ->
            Encode.object
                [ ( "@context", Encode.string "https://www.w3.org/ns/activitystreams" )
                , ( "id", Encode.string "https://joshi.monster/.activitypub/outbox" )
                , ( "type", Encode.string "OrderedCollection" )
                , ( "totalItems", Encode.int 0 )
                , ( "orderedItems", Encode.list never [] )
                ]
                |> Encode.encode 0
                |> Server.Response.body
                |> Server.Response.withHeader "Content-Type" "application/activity+json"
                |> BackendTask.succeed
        )
        |> ApiRoute.literal ".activitypub"
        |> ApiRoute.slash
        |> ApiRoute.literal "outbox"
        |> ApiRoute.serverRender
