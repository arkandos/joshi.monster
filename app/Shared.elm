module Shared exposing (Data, Model, Msg(..), template)

import BackendTask exposing (BackendTask)
import Effect exposing (Effect)
import FatalError exposing (FatalError)
import Html exposing (Attribute, Html, a, br, div, p, strong, text)
import Html.Attributes as HA exposing (class, href)
import Html.Events exposing (onClick)
import Icons
import Json.Decode as Json exposing (Decoder)
import Pages.Flags
import Pages.PageUrl exposing (PageUrl)
import Route exposing (Route)
import SharedTemplate exposing (SharedTemplate)
import Ui
import UrlPath exposing (UrlPath)
import View exposing (View)


template : SharedTemplate Msg Model Data msg
template =
    { init = init
    , update = update
    , view = view
    , data = data
    , subscriptions = subscriptions
    , onPageChange = Just OnPageChange
    }


type Msg
    = ToggleColorScheme
    | ShowAntiAdBlock
    | OnPageChange { path : UrlPath, query : Maybe String, fragment : Maybe String }


type alias Data =
    ()


type ColorScheme
    = Light
    | Dark


setColorScheme : ColorScheme -> Effect msg
setColorScheme colorScheme =
    case colorScheme of
        Light ->
            Effect.setDocumentClass "light"

        Dark ->
            Effect.setDocumentClass "dark"


type alias Model =
    { colorScheme : ColorScheme
    , showAntiAdBlock : Bool
    }


type alias Flags =
    { prefersDarkMode : Bool
    , userAgent : String
    }


defaultFlags : Flags
defaultFlags =
    { prefersDarkMode = False
    , userAgent = ""
    }


flagsDecoder : Decoder Flags
flagsDecoder =
    Json.map2 Flags
        (Json.field "prefersDarkMode" Json.bool)
        (Json.field "userAgent" Json.string)


init :
    Pages.Flags.Flags
    ->
        Maybe
            { path :
                { path : UrlPath
                , query : Maybe String
                , fragment : Maybe String
                }
            , metadata : route
            , pageUrl : Maybe PageUrl
            }
    -> ( Model, Effect Msg )
init maybeFlags maybePagePath =
    let
        flags =
            case maybeFlags of
                Pages.Flags.PreRenderFlags ->
                    defaultFlags

                Pages.Flags.BrowserFlags browserFlags ->
                    Json.decodeValue flagsDecoder browserFlags
                        |> Result.withDefault defaultFlags

        colorScheme =
            if flags.prefersDarkMode then
                Dark

            else
                Light
    in
    ( { colorScheme = colorScheme
      , showAntiAdBlock = False
      }
    , Effect.batch
        [ setColorScheme colorScheme
        , if not (userAgentIsMobile flags.userAgent) then
            Effect.nextTick ShowAntiAdBlock

          else
            Effect.none
        ]
    )


update : Msg -> Model -> ( Model, Effect Msg )
update msg model =
    case msg of
        ToggleColorScheme ->
            let
                newColorScheme =
                    case model.colorScheme of
                        Dark ->
                            Light

                        Light ->
                            Dark
            in
            ( { model | colorScheme = newColorScheme }
            , setColorScheme newColorScheme
            )

        ShowAntiAdBlock ->
            ( { model | showAntiAdBlock = True }, Effect.none )

        OnPageChange evt ->
            ( model, Effect.onPageChange evt )


subscriptions : UrlPath -> Model -> Sub Msg
subscriptions _ _ =
    Sub.none


data : BackendTask FatalError Data
data =
    BackendTask.succeed ()


view :
    Data
    ->
        { path : UrlPath
        , route : Maybe Route
        }
    -> Model
    -> (Msg -> msg)
    -> View msg
    -> { body : List (Html msg), title : String }
view sharedData page model toMsg pageView =
    { body =
        [ Html.button
            [ class "color-scheme-btn"
            , ariaHidden True
            , onClick (toMsg ToggleColorScheme)
            ]
            [ case model.colorScheme of
                Dark ->
                    Icons.moon

                Light ->
                    Icons.sun
            ]
        , if model.showAntiAdBlock then
            antiAdBlock

          else
            text ""
        , Html.main_ [] pageView.body
        , footer
        ]
    , title = pageView.title
    }


footer : Html msg
footer =
    Html.footer []
        [ Ui.ul [ class "link-list" ]
            [ a [ href "https://gitlab.com/arkandos/localhostd" ] [ text "localhostd" ]
            , a [ href "https://ip.joshi.monster" ] [ text "ip" ]
            , a [ href "https://cam.joshi.monster" ] [ text "cam" ]
            , a [ href "https://share.joshi.monster" ] [ text "share" ]
            , a [ href "https://proxy.joshi.monster" ] [ text "proxy" ]
            ]
        , Route.link [] [ text "Impressum" ] Route.Impressum
        ]


antiAdBlock : Html msg
antiAdBlock =
    div [ class "ftf-dma-note ytd-j yxd-j yxd-jd aff-content-col aff-inner-col aff-item-list ark-ad-message inplayer-ad inplayer_banners in_stream_banner trafficjunky-float-right dbanner preroll-blocker happy-inside-player blocker-notice blocker-overlay exo-horizontal ave-pl bottom-hor-block brs-block advboxemb wgAdBlockMessage glx-watermark-container overlay-advertising-new header-menu-bottom-ads rkads mdp-deblocker-wrapper amp-ad-inner imggif bloc-pub bloc-pub2 hor_banner aan_fake aan_fake__video-units rps_player_ads fints-block__row full-ave-pl full-bns-block vertbars video-brs player-bns-block wps-player__happy-inside gallery-bns-bl stream-item-widget adsbyrunactive happy-under-player adde_modal_detector adde_modal-overlay ninja-recommend-block aoa_overlay message" ]
        [ p []
            [ strong [] [ text "No Ad-blocker detected" ]
            , text " - "
            , text "Consider installing a browser extension that blocks ads and other malicious scripts in your browser to protect your privacy and security."
            , br [] []
            , Ui.ul [ class "link-list" ]
                [ Ui.externalLink "https://ublockorigin.com/" "uBlock Origin"
                , Ui.externalLink "https://adblockplus.org/" "AdBlock Plus"
                , Ui.externalLink "https://www.ghostery.com/" "Ghostery"
                , Ui.externalLink "https://adguard.com/en/welcome.html" "AdGuard"
                ]
            ]
        ]


ariaHidden : Bool -> Attribute msg
ariaHidden value =
    let
        valueStr =
            if value then
                "true"

            else
                "false"
    in
    HA.attribute "aria-hidden" valueStr


userAgentIsMobile : String -> Bool
userAgentIsMobile userAgent =
    -- Adapted from wp_is_mobile
    let
        patterns =
            [ "Mobile"
            , "Android"
            , "Silk/"
            , "Kindle"
            , "BlackBerry"
            , "Opera Mini"
            , "Opera Mobi"
            ]
    in
    List.any (\pattern -> String.contains pattern userAgent) patterns
