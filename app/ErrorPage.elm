module ErrorPage exposing (ErrorPage(..), Model, Msg, head, init, internalError, notFound, statusCode, update, view)

import Config
import Effect exposing (Effect)
import Head
import Html exposing (Html, p, text)
import Html.Attributes exposing (class)
import Ui
import View exposing (View)


type alias Msg =
    ()


type alias Model =
    ()


init : ErrorPage -> ( Model, Effect Msg )
init errorPage =
    ( ()
    , Effect.none
    )


update : ErrorPage -> Msg -> Model -> ( Model, Effect Msg )
update errorPage msg model =
    ( model, Effect.none )


head : ErrorPage -> List Head.Tag
head errorPage =
    []


type ErrorPage
    = NotFound
    | InternalError String


notFound : ErrorPage
notFound =
    NotFound


internalError : String -> ErrorPage
internalError =
    InternalError


view : ErrorPage -> Model -> View Msg
view error model =
    { title = Config.name
    , body =
        [ Ui.h1 [ class "highlight" ]
            [ text "404"
            ]
        , p [ class "lead" ]
            [ text "The answers you seek are nowhere to be found."
            ]
        ]
    }


statusCode : ErrorPage -> number
statusCode error =
    case error of
        NotFound ->
            404

        InternalError _ ->
            500
