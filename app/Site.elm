module Site exposing (config)

import BackendTask exposing (BackendTask)
import Config
import FatalError exposing (FatalError)
import Head
import SiteConfig exposing (SiteConfig)


config : SiteConfig
config =
    { canonicalUrl = Config.canonicalUrl
    , head = head
    }


head : BackendTask FatalError (List Head.Tag)
head =
    BackendTask.succeed
        [ Head.metaName "viewport" (Head.raw "width=device-width,initial-scale=1")
        , Head.sitemapLink "/sitemap.txt"
        , Head.rssLink "/rss.xml"
        ]
