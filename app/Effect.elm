port module Effect exposing
    ( Effect
    , batch
    , copyToClipboard
    , fromCmd
    , getViewportSize
    , map
    , nextTick
    , none
    , onPageChange
    , perform
    , setDocumentClass
    )

import Browser.Dom
import Browser.Navigation as Nav
import Http
import Pages.Fetcher exposing (Fetcher)
import Task
import Url exposing (Url)
import UrlPath exposing (UrlPath)


port onUrlChange : { path : String, query : Maybe String, fragment : Maybe String } -> Cmd msg


port setDocumentClassName : String -> Cmd msg


port clipboardWriteText : String -> Cmd msg


type Effect msg
    = None
    | Batch (List (Effect msg))
    | FromCmd (Cmd msg)
    | GetViewportSize (Int -> Int -> msg)
    | SetDocumentClass String
    | CopyToClipboard String
    | OnPageChange { path : UrlPath, query : Maybe String, fragment : Maybe String }
    | NextTick msg


type alias PerformCapabilities a b msg pageMsg =
    { fetchRouteData :
        { data : Maybe a
        , toMsg : Result Http.Error Url -> pageMsg
        }
        -> Cmd msg
    , submit :
        { values : b
        , toMsg : Result Http.Error Url -> pageMsg
        }
        -> Cmd msg
    , runFetcher : Fetcher pageMsg -> Cmd msg
    , fromPageMsg : pageMsg -> msg
    , key : Nav.Key
    , setField : { formId : String, name : String, value : String } -> Cmd msg
    }


none : Effect msg
none =
    None


batch : List (Effect msg) -> Effect msg
batch =
    Batch


fromCmd : Cmd msg -> Effect msg
fromCmd =
    FromCmd


getViewportSize : (Int -> Int -> msg) -> Effect msg
getViewportSize =
    GetViewportSize


setDocumentClass : String -> Effect msg
setDocumentClass =
    SetDocumentClass


copyToClipboard : String -> Effect msg
copyToClipboard =
    CopyToClipboard


onPageChange : { path : UrlPath, query : Maybe String, fragment : Maybe String } -> Effect msg
onPageChange =
    OnPageChange


nextTick : msg -> Effect msg
nextTick =
    NextTick


map : (a -> b) -> Effect a -> Effect b
map f effect =
    case effect of
        None ->
            None

        Batch nested ->
            Batch (List.map (map f) nested)

        FromCmd cmd ->
            FromCmd (Cmd.map f cmd)

        GetViewportSize toMsg ->
            GetViewportSize (\w h -> f (toMsg w h))

        SetDocumentClass class ->
            SetDocumentClass class

        CopyToClipboard str ->
            CopyToClipboard str

        OnPageChange evt ->
            OnPageChange evt

        NextTick msg ->
            NextTick (f msg)


perform : PerformCapabilities a b msg pageMsg -> Effect pageMsg -> Cmd msg
perform ({ fromPageMsg } as capabilities) effect =
    case effect of
        None ->
            Cmd.none

        Batch effects ->
            Cmd.batch (List.map (perform capabilities) effects)

        FromCmd cmd ->
            Cmd.map fromPageMsg cmd

        GetViewportSize toMsg ->
            Browser.Dom.getViewport
                |> Task.map (\{ viewport } -> toMsg (floor viewport.width) (floor viewport.height))
                |> Task.perform fromPageMsg

        SetDocumentClass class ->
            setDocumentClassName class

        CopyToClipboard str ->
            clipboardWriteText str

        OnPageChange { path, fragment, query } ->
            onUrlChange
                { path = UrlPath.toAbsolute path
                , fragment = fragment
                , query = query
                }

        NextTick msg ->
            Browser.Dom.getViewport
                |> Task.map (always msg)
                |> Task.perform fromPageMsg
