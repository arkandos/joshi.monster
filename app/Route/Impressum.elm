module Route.Impressum exposing (ActionData, Data, Model, Msg, route)

import Base exposing (BackendTask, FatalError, Head, StaticPage, View)
import Config
import Content exposing (Text)
import Html exposing (..)
import Html.Attributes exposing (class)
import Route
import Ui


type alias Model =
    {}


type alias Msg =
    ()


type alias Data =
    Text


type alias ActionData =
    {}


route : StaticPage Data
route =
    Base.staticPage
        { head = head
        , data = dataTask
        , view = view
        }


dataTask : BackendTask FatalError Data
dataTask =
    Content.loadMarkdownText "content/impressum.md"


head : Data -> Head
head data =
    Ui.head
        { title = "Impressum"
        , description = Config.name ++ " Impressum"
        }


view : Data -> View Never
view data =
    { title = "Impressum"
    , body =
        [ Ui.h1 [ class "highlight" ] [ text "Impressum" ]
        , h2 [] [ text "German law requires every website owner to dox themselves." ]
        , Content.view
            { page = Route.Impressum
            , content = data
            , attributes = []
            , copyToClipboard = Nothing
            }
        ]
    }
