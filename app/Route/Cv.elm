module Route.Cv exposing (ActionData, Data, Model, Msg, route)

import Base exposing (BackendTask, FatalError, Head, StaticPage, View)
import Content exposing (Text)
import Html exposing (..)
import Html.Attributes exposing (class)
import Route
import Ui


type alias Model =
    {}


type alias Msg =
    ()


type alias Data =
    Text


type alias ActionData =
    {}


route : StaticPage Data
route =
    Base.staticPage
        { head = head
        , data = dataTask
        , view = view
        }


dataTask : BackendTask FatalError Data
dataTask =
    Content.loadMarkdownText "content/cv.md"


head : Data -> Head
head data =
    Ui.head
        { title = "Joshua Reusch - CV / Resume"
        , description = "Joshua Reusch - CV / Resume"
        }


view : Data -> View Never
view data =
    { title = "Joshua Reusch - CV / Resume"
    , body =
        [ Ui.h1 [ class "highlight" ] [ text "Resume" ]
        , Content.view
            { page = Route.Cv
            , content = data
            , attributes = []
            , copyToClipboard = Nothing
            }
        ]
    }
