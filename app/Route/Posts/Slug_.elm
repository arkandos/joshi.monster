module Route.Posts.Slug_ exposing (ActionData, Data, Model, Msg, route)

import BackendTask
import Base exposing (BackendTask, FatalError, Head, StatefulRoute, StatelessRoute, View)
import Content exposing (Post)
import Effect exposing (Effect)
import Html exposing (..)
import Html.Attributes exposing (class, id)
import Route
import Ui


type alias Model =
    ()


type Msg
    = CopyToClipboard String


type alias RouteParams =
    { slug : String }


type alias Data =
    Post


type alias ActionData =
    {}


route : StatefulRoute RouteParams Data ActionData Model Msg
route =
    Base.dynamicPages
        { head = head
        , pages = pages
        , data = getData
        , init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


pages : BackendTask FatalError (List RouteParams)
pages =
    Content.listSlugs
        |> BackendTask.map (List.map (\{ slug } -> RouteParams slug))


getData : RouteParams -> BackendTask FatalError Data
getData { slug } =
    Content.loadPost slug


head : RouteParams -> Data -> Head
head _ data =
    Ui.head
        { title = data.metadata.title
        , description = data.metadata.summary
        }


init : RouteParams -> Data -> ( Model, Effect Msg )
init _ _ =
    ( (), Effect.none )


update : RouteParams -> Data -> Msg -> Model -> ( Model, Effect Msg )
update _ _ msg model =
    case msg of
        CopyToClipboard str ->
            ( model, Effect.copyToClipboard str )


view : RouteParams -> Data -> Model -> View Msg
view params data _ =
    { title = data.metadata.title
    , body =
        [ Ui.h1
            [ id data.metadata.slug
            , class "highlight"
            ]
            [ text data.metadata.title ]
        , Content.view
            { page = Route.Posts__Slug_ params
            , content = data.text
            , attributes = []
            , copyToClipboard = Just CopyToClipboard
            }
        ]
    }


subscriptions : RouteParams -> Model -> Sub msg
subscriptions _ _ =
    Sub.none
