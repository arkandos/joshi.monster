module Route.ThankYou exposing (ActionData, Data, Model, Msg, route)

import Base exposing (App, DynamicPage, Effect, Head, PagesMsg, SharedModel, UrlPath, View)
import Dict
import Html exposing (..)
import Html.Attributes exposing (class)
import ParticleSystem exposing (Particle, ParticleSystem)
import Svg exposing (Svg)
import Svg.Attributes as SA
import Ui


type alias Model =
    { firstName : Maybe String
    , ps : ParticleSystem
    }


type Msg
    = GotParticleSystemMsg ParticleSystem.Msg


type alias RouteParams =
    {}


type alias Data =
    {}


type alias ActionData =
    {}


route : DynamicPage Data Model Msg
route =
    Base.single { head = head, data = Base.noData }
        |> Base.buildWithLocalState
            { init = init
            , update = update
            , subscriptions = subscriptions
            , view = view
            }


init : App Data ActionData RouteParams -> SharedModel -> ( Model, Effect Msg )
init app _ =
    let
        firstName =
            app.url
                |> Maybe.map .query
                |> Maybe.andThen (Dict.get "first_name")
                |> Maybe.andThen List.head

        ( ps, psCmd ) =
            ParticleSystem.init
                { toMsg = GotParticleSystemMsg
                , spawnFrequency = ParticleSystem.constant 0.1
                , x = ParticleSystem.random -30 -10
                , y = ParticleSystem.constant 0
                , a = ParticleSystem.random (degrees 0) (degrees 360)
                , dx = ParticleSystem.random -15 15
                , dy = ParticleSystem.random 30 50
                , da = ParticleSystem.random (degrees -5) (degrees 5)
                , ddx = ParticleSystem.random -0.5 0.5
                , ddy = ParticleSystem.random -0.5 0.5
                , dda = ParticleSystem.constant 0
                , lifespan = ParticleSystem.random 2 3
                }
    in
    ( { ps = ps
      , firstName = firstName
      }
    , Base.fromCmd psCmd
    )


update : App Data ActionData RouteParams -> SharedModel -> Msg -> Model -> ( Model, Effect Msg )
update _ _ msg model =
    case msg of
        GotParticleSystemMsg psMsg ->
            ( { model | ps = ParticleSystem.update psMsg model.ps }, Base.noEffect )


subscriptions : RouteParams -> UrlPath -> SharedModel -> Model -> Sub Msg
subscriptions _ _ _ model =
    ParticleSystem.subscriptions GotParticleSystemMsg model.ps


head : App Data ActionData RouteParams -> Head
head _ =
    Ui.head
        { description = "A thank-you letter to all my donors."
        , title = "Thank you!"
        }


view : App Data ActionData RouteParams -> SharedModel -> Model -> View (PagesMsg Msg)
view _ _ model =
    { title = "Thank you!"
    , body =
        [ viewParticles model
        , Svg.svg
            [ SA.width "300"
            , SA.viewBox "0 7.3 24 14.3"
            , SA.stroke "currentColor"
            , SA.fill "currentColor"
            , SA.strokeWidth "1.3"
            , SA.strokeLinecap "round"
            , SA.strokeLinejoin "round"
            , SA.class "coffee"
            ]
            [ Svg.path [ SA.d "M18 8h1a4 4 0 0 1 0 8h-1", SA.fill "none" ] []
            , Svg.path [ SA.d "M2 8h16v9a4 4 0 0 1-4 4H6a4 4 0 0 1-4-4V8z" ] []
            ]
        , article []
            [ h1 [] [ text "Thank you so much 😊" ]
            , case model.firstName of
                Just firstName ->
                    h3 [] [ text "Dear ", text firstName, text "," ]

                Nothing ->
                    h3 [] [ text "Hey kind soul," ]
            , p []
                [ text "these are difficult times for us all, so I appreciate it even more that you decided to sponsor me as well! It makes me incredibly happy to know my work has been useful to someone else. You are valid, special, and I will always keep you in my heart."
                ]
            , p []
                [ text "If I can I would love to help you out as well! If you have any question, a feature request or just want to talk, feel free to DM me anytime. I'm most easily reached over "
                , Ui.externalLink "https://layer8.space/@jreusch" "Mastodon"
                , text " or, if you prefer, "
                , Ui.externalLink "mailto:jreusch4+thankyou@gmail.com" "plain-old e-mail"
                , text "."
                ]
            , p []
                [ text "Take care,", br [] [], text "joshua 💜" ]
            ]
        ]
    }


viewParticles : Model -> Html msg
viewParticles model =
    Svg.svg
        [ SA.width "300"
        , SA.height "200"
        , SA.viewBox <| "0 0 300 200"
        , SA.fill "currentColor"
        , SA.class "coffee-smoke"
        ]
        [ Svg.g
            [ SA.transform <| "translate(150, 200) scale(1, -1)"
            ]
            (List.map viewParticle model.ps.particles)
        ]


viewParticle : Particle -> Svg msg
viewParticle p =
    let
        opacity =
            clamp 0 0.8 (min p.age p.lifespan)

        scale =
            p.da * 4
    in
    Svg.g
        [ SA.transform <| "translate(" ++ String.fromFloat p.x ++ "," ++ String.fromFloat p.y ++ ") rotate(" ++ String.fromFloat (p.a * 180 / pi) ++ ") scale(" ++ String.fromFloat scale ++ ")"
        , SA.opacity <| String.fromFloat opacity
        ]
        [ Svg.path
            [ SA.d "M62.305368692688 7.7833364299924a25 25 0 0 1 29.389262614624 0l51.512436822871 37.425976073005a25 25 0 0 1 9.081781600067 27.950849718747l-19.676000023005 60.556501348263a25 25 0 0 1 -23.776412907379 17.274575140626l-63.672873599732 0a25 25 0 0 1 -23.776412907379 -17.274575140626l-19.676000023005 -60.556501348263a25 25 0 0 1 9.081781600067 -27.950849718747"
            , SA.strokeLinejoin "round"
            , SA.strokeWidth "20"
            ]
            []
        ]
