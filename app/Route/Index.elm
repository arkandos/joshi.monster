module Route.Index exposing (ActionData, Data, Model, Msg, route)

import BackendTask
import Base exposing (BackendTask, FatalError, Head, StaticPage, View)
import Config
import Content exposing (Content(..))
import Date
import Html exposing (..)
import Html.Attributes as HA exposing (class)
import Route
import Ui


type alias Model =
    {}


type alias Msg =
    ()


type alias Data =
    List Content


type alias ActionData =
    {}


route : StaticPage Data
route =
    Base.staticPage
        { head = head
        , data = dataTask
        , view = view
        }


dataTask : BackendTask FatalError Data
dataTask =
    Content.listAll
        |> BackendTask.map
            (\data ->
                data
                    |> List.filter
                        (\content ->
                            case content of
                                PostContent _ ->
                                    True

                                LinkContent { highlight } ->
                                    highlight
                        )
                    |> List.sortWith
                        (\c1 c2 -> Date.compare (Content.publishedAt c2) (Content.publishedAt c1))
                    |> List.take 4
            )


head : Data -> Head
head _ =
    Ui.head
        { title = Config.name
        , description = Config.description
        }


view : Data -> View msg
view data =
    { title = Config.name
    , body =
        [ Ui.h1 [ class "f4 highlight" ]
            [ text "Hi there, I'm joshi~"
            ]
        , p [ class "lead" ]
            [ text "I help move ✨digitalization✨ forward at "
            , Ui.externalLink "https://www.creativity-gmbh.de" "creativITy GmbH"
            , text ", make lots of useless software using tools nobody cares about, and dream about game design, music, decentralization, and owning my own coffee shop."
            ]
        , p [] [ text "I'm also currently a student again, learning more about AI and data science." ]
        , h2 [] [ text "Recently I did:" ]
        , ul []
            (List.map (Content.viewContentLink { withSummary = False }) data)
        , p []
            [ text "If you want a more complete overview of what I was up to for the last year or so, I've collected a "
            , Route.Posts |> Route.link [] [ text "list of all GitLab projects and posts here" ]
            , text ". I haven't gotten around to sorting all my all backups yet, so older stuff is still missing, though!"
            -- , text " If you are more interested in my professional history, "
            -- , Route.Cv |> Route.link [] [ text "I have that available as well." ]
            ]
        , p []
            [ text "Feel free to complain about the lack of activity on my "
            , Ui.externalLink "https://gitlab.com/arkandos" "Gitlab"
            , text ", watch me ramble on "
            , a
                [ HA.href "https://layer8.space/@jreusch"
                , HA.target "_blank"
                , HA.rel "me"
                ]
                [ text "Mastodon" ]
            , text ", or send me an "
            , Ui.externalLink "mailto:jreusch4+monster@gmail.com" "e-mail"
            , text " of your Elden Ring lore theory."
            ]
        ]
    }
