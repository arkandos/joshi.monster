module Route.Posts exposing (ActionData, Data, Model, Msg, route)

import Base exposing (BackendTask, FatalError, Head, StaticPage, View)
import Content exposing (Content(..))
import Date
import Html exposing (Html, h2, li, p, text, ul)
import Html.Attributes exposing (class)
import List.Extra as List
import Ui


type alias Model =
    {}


type alias Msg =
    ()


type alias Data =
    List Content


type alias ActionData =
    {}


route : StaticPage Data
route =
    Base.staticPage
        { head = head
        , data = dataTask
        , view = view
        }


dataTask : BackendTask FatalError Data
dataTask =
    Content.listAll


head : Data -> Head
head _ =
    Ui.head
        { title = "Posts"
        , description = "A (mostly) complete list of things I've published and done"
        }


view : Data -> View Never
view data =
    let
        groupedData =
            data
                |> List.sortWith
                    (\c1 c2 ->
                        Date.compare (Content.publishedAt c2) (Content.publishedAt c1)
                    )
                |> List.groupWhile
                    (\c1 c2 ->
                        let
                            published1 =
                                Content.publishedAt c1

                            published2 =
                                Content.publishedAt c2
                        in
                        Date.month published1 == Date.month published2 && Date.year published1 == Date.year published2
                    )
    in
    { title = "Things I've done"
    , body =
        Ui.h1 [ class "highlight" ] [ text "Things I've done" ]
            :: p [ class "lead" ]
                [ text "I've been programming and on the internet for over 15 years now, so I have lots of unfinished and finished work just lying around in some backup waiting to be rediscovered. For now, I've linked things that is currently on my "
                , Ui.externalLink "https://gitlab.com/arkandos" "GitLab"
                , text "."
                ]
            :: List.concatMap viewGroup groupedData
    }


viewGroup : ( Content, List Content ) -> List (Html msg)
viewGroup ( first, rest ) =
    [ h2 [] [ text <| Date.format "MMMM, y" (Content.publishedAt first) ]
    , ul [] (List.map (Content.viewContentLink { withSummary = True }) (first :: rest))
    ]
