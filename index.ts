declare global {
  interface Window {
    goatcounter?: {
      count(vars?: {
        path?: string | ((path: string) => string),
        referrer?: string | ((referrer: string) => string),
        title?: string | ((title: string) => string),
        event?: boolean
      }): void
    }
  }
}

interface OutgoingPort<T> {
  subscribe(callback: (value: T) => void): void
}

interface IncomingPort<T> {
  send(value: T): void
}

interface App {
  ports: {
    setDocumentClassName: OutgoingPort<string>
    onUrlChange: OutgoingPort<{ path: string, query: string | null, fragment: string | null }>
    clipboardWriteText: OutgoingPort<string>
  }
}

interface ElmPagesInit {
  load: (elmLoaded: Promise<App>) => Promise<void>;
  flags: () => unknown;
}

const config: ElmPagesInit = {
  async load(elmLoaded) {
    const app = await elmLoaded

    app.ports.setDocumentClassName.subscribe(bodyClass => {
      document.documentElement.className = bodyClass
    })

    app.ports.onUrlChange.subscribe(() => {
      window.goatcounter?.count()
    })

    app.ports.clipboardWriteText.subscribe((str) => {
      navigator.clipboard.writeText(str)
    })
  },

  flags() {
    const { matches: prefersDarkMode } = matchMedia('(prefers-color-scheme: dark)')
    const userAgent = navigator.userAgent
    return {
      prefersDarkMode,
      userAgent
    }
  },
};

export default config;
