import * as path from 'node:path'
import { defineConfig } from "vite";
import { viteStaticCopy } from 'vite-plugin-static-copy';
import adapter from "elm-pages/adapter/netlify.js";

const appContentDir = process.env.APP_CONTENT_DIR
if (!appContentDir) {
  console.error("No APP_CONTENT_DIR")
  process.exit(1)
}

export default {
  vite: defineConfig({
    // https://github.com/dillonkearns/elm-pages/issues/447
    assetsInclude: ['/elm-pages.js'],

    plugins: [
      viteStaticCopy({
        // structured: true,
        
        targets: appContentDir.split(':').map(contentDir => (
          {
            src: './' + path.join(contentDir, '*/!(post.md|)'),
            dest: './posts/',

            rename(fileName, fileExt, filePath) {
              console.log('copy', filePath)
              return path.relative(contentDir, filePath)
            }
          }
        ))
      })
    ],

    build: {
      rollupOptions: {
        output: {
          assetFileNames: 'assets/[name].[ext]'
        }
      }
    }
  }),

  adapter,

  headTagsTemplate(context) {
    return `
      <link rel="stylesheet" href="/css/index.css" />
      <meta name="generator" content="elm-pages v${context.cliVersion}" />
      <script data-goatcounter="https://arkandos.goatcounter.com/count" async src="/count.js"></script>
    `
  },
  preloadTagForFile(file) {
    // add preload directives for JS assets and font assets, etc., skip for CSS files
    // this function will be called with each file that is procesed by Vite, including any files in your headTagsTemplate in your config
    return !file.endsWith(".css");
  },
};
